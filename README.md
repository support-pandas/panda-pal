# Panda Pal #

## What is this repository for? ##

* This project is intended to be a general easy-to-use wrapper and toolkit for the Canvas API.

## How do I get set up? ##
### Cloning Panda Pal ###
* Clone the Git repository to obtain the source code:
```
git clone https://bitbucket.org/support-pandas/panda-pal.git
```

### Setting up Ruby ###
* Check your version of Ruby by entering `ruby --version` in a terminal.
* If your version is not `2.1.2`, double click the `install_ruby.command` file.

### Setting up Panda Pal ###
* Double click the `setup.command` file.
* Read the prompts and answer the questions to configure your Panda Pal.

### Running Panda Pal ###
* Double click the `canvas_toolkit.command` file.

### Additional information ###
* More information about keeping the repo updated, and helpful use steps can be found on the wiki.
* [Updating and first launch steps](https://bitbucket.org/support-pandas/panda-pal/wiki/Getting%20Started/Updating%20and%20first%20launch%20steps)

### Using Panda Pal in other Scripts ###
* All scripts that leverage the functionality of this repo will need to require the core library file.

```
require './lib/canvas_toolkit.rb'
```

## Overview ##
* By default, you can set a default domain to execute commands against (in etc/config.yml).  If this needs to be changed during runtime, the 'set_domain' method can be used.

```
set_domain 'canvas.instructure.com'
```
* Some objects (like courses) can be loaded in this manner

```
course = Course.find 1234
```
* Other objects will require inheritance (like assignments, quizzes, etc).  For these, helper methods are present in the parent object.

```
course = Course.find 1234
assignment = course.find_assignment 4321
```
* Attributes can be modified using direct assignment

```
course = Course.find 1234
course.name = 'new name'
```
* When updating attributes, the changes are not yet pushed to Canvas, and are instead staged locally.  To view the pending changes, 'staged' can be used.

```
course.staged
 => {"name"=>"new name"} 
```
* If there are changes that are staged but they shouldn't be pushed, the changes can be reset

```
course.reset
```
* If changes are staged and ready to be pushed, the object can be pushed using the update method.

```
course.update
```
* If you wish to delete the item, it can be done so with the delete method.  NOTE: This method will ask for confirmation if safe mode is enabled (which it is by default, see etc/config.yml).

```
course.delete
```