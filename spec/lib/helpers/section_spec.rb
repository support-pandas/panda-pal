require './spec/spec_helper'

describe Section do
  before :all do
    course
    section
  end

  it 'should find a section' do
    s = Section.find @section.id
    expect(s).to_not be_nil
    expect(s.id).to eql @section.id
  end

  it 'should update a section' do
    n = SecureRandom.uuid
    @section.update({ :name => n })
    expect(@section.name).to eql n
  end

  it 'should be able to be deleted' do
    id = @section.id
    expect(@section.destroy!).to be_truthy
    expect { Section.find id }.to raise_error(CanvasToolkit::CanvasClientError)
  end

  describe 'crosslisting' do
    before :all do
      course
      section
      @course2 = Course.create!
    end

    it 'should be crosslist-able' do
      @section.crosslist @course2
      expect(@section.crosslisted?).to be_truthy
      expect(@section.course_id).to eql @course2.id
    end

    it 'should be decrosslist-able' do
      @section.decrosslist
      expect(@section.crosslisted?).to be_falsey
    end
  end

end
