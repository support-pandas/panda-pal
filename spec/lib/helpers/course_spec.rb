require './spec/spec_helper'

describe Course do

  describe 'course creation actions' do
    it 'should include a new status when creating a local course' do
      c = Course.create
      expect(c.additional_attributes[:status]).to eql 'new'
    end

    it 'should receive an id when pushed to Canvas' do
      c = Course.create!
      expect(c.id).to_not be_nil
    end

    it 'should include given attributes during creation' do
      n = 'A course name'
      c = Course.create!({ :name => n })
      expect(c.name).to eql n
    end
  end

  describe 'existing course actions' do
    before :each do
      course
    end

    it 'should publish a course' do
      @course.publish
      @course.update
      expect(@course.workflow_state).to eql 'available'
    end

    it 'should unpublish a course' do
      @course.publish
      @course.update
      @course.unpublish
      @course.update
      expect(@course.workflow_state).to eql 'unpublished'
    end

    it 'should find courses' do
      c = Course.find @course.id
      expect(c.id).to eql @course.id
    end

    it 'should allow attributes to be updated' do
      n = 'Some new name for the course'
      @course.name = n
      @course.update
      expect(@course.name).to eql n
    end

    it 'should allow the course to be deleted' do
      id = @course.id
      expect(@course.destroy!).to be_truthy
      expect { Course.find id }.to raise_error(CanvasToolkit::CanvasClientError)
    end

    it 'should allow attributes to be staged while updating with a given hash' do
      n = 'Some new name for the course'
      @course.update({ :name => n })
      expect(@course.name).to eql n
    end

    it 'should allow attributes to be staged while updating if given a block' do
      n = 'Some new name for the course'
      @course.update { |a| a.name = n }
      expect(@course.name).to eql n
    end

    # See https://canvas.instructure.com/doc/api/courses.html#method.courses.update_settings
    it 'should be able to update settings' do
      @course.update_settings({ 'allow_student_discussion_topics' => false })
      expect(@course.settings['allow_student_discussion_topics']).to be_falsey
    end
  end

  describe 'assignment related actions in courses' do
    before :all do
      course
    end
    before :each do
      assignment({:name => 'assignment_name'})
    end

    it 'should create an assignment' do
      a = @course.create_assignment! ({:name => 'assignment_name'})
      expect(a).to_not be_nil
      expect(a.id).to_not be_nil
    end

    it 'should return an array of assignments in a course' do
      a = @course.assignments
      expect(a.length).to be_between(1, 1000)
      expect(a.first.id).to_not be_nil
    end

    it 'should find an assignment' do
      a = @course.find_assignment @assignment.id
      expect(a).to_not be_nil
      expect(a.id).to_not be_nil
    end

    it 'should return an array of assignment groups' do
      ag = @course.assignment_groups
      expect(ag.length).to be_between(1, 1000)
      expect(ag.first.assignments.length).to be_between(1, 1000)
    end
  end

  describe 'quiz related actions in course' do
    before :all do
      course
    end
    before :each do
      quiz({:title => 'quiz_name'})
    end

    it 'should create an quiz' do
      a = @course.create_quiz! ({:title => 'quiz_name'})
      expect(a).to_not be_nil
      expect(a.id).to_not be_nil
    end

    it 'should return an array of quizzes in a course' do
      a = @course.quizzes
      expect(a.length).to be_between(1, 1000)
      expect(a.first.id).to_not be_nil
    end

    it 'should find a quiz' do
      a = @course.find_quiz @quiz.id
      expect(a).to_not be_nil
      expect(a.id).to_not be_nil
    end
  end

  describe 'user related actions in courses' do
    before :all do
      course
    end

    it 'should return an array of users in a course' do
      expect(@course.users).to_not be_nil
    end
  end

  describe 'course enrollments' do
    before :all do
      course_with_student({ :enrollment_state => 'active' })
    end

    it 'should return an array of enrollments for a given course' do
      expect(@course.enrollments.length).to eql 1
    end
  end

  describe 'file related actions' do
    before :all do
      course
      tmp_file
    end

    it 'should upload a file to a course' do
      f = @course.upload_file(@file)
      expect(f.id).to_not be_nil
    end

    it 'should return an array of files in a course' do
      f = @course.upload_file(@file)
      expect(@course.files).to be_a(Batch)
      expect(@course.files.length).to be_between(1, 100)
    end
  end

  describe 'folder related actions' do
    before :all do
      course
    end

    it 'should create a folder' do
      f = @course.create_folder!
      expect(f).to_not be_nil
      expect(f.id).to_not be_nil
    end

    it 'should find a folder' do
      f = @course.create_folder!
      nf = @course.find_folder f.id
      expect(nf).to_not be_nil
      expect(nf.id).to eql f.id
    end
  end

  describe 'sections' do
    before :all do
      course
    end

    it 'should create a section' do
      s = @course.create_section!({ :name => SecureRandom.uuid })
      expect(s).to_not be_nil
      expect(s.id).to_not be_nil
    end

    describe 'existing sections' do
      before :all do
        section
      end

      it 'should return an array of sections in a course' do
        s = @course.sections
        expect(s).to_not be_nil
        expect(s.length).to be_between(1, 100)
      end

      it 'should find a section in a course' do
        id = @section.id
        s = @course.find_section id
        expect(s).to_not be_nil
        expect(s.id).to eql id
      end
    end
  end

  describe 'content migrations and exports' do

    before :all do
      course
    end

    describe 'common cartridge exports' do
      before :all do
        @export = course.export
      end

      it 'should return a ContentExport object' do
        expect(@export).to be_a CanvasToolkit::ContentExport
      end

      it 'should allow for progress checking on ContentExport object' do

      end

      it 'should allow for downloading complete export file from ContentExport object' do

      end

    end
  end
end
