require './spec/spec_helper'

describe CanvasFile do
  before :all do
    # Define a course, just to establish a context
    course
    tmp_file
    @uploaded_file = @course.upload_file @file
  end

  it 'should find a file' do
    f = CanvasFile.find(@uploaded_file.id)
    expect(f).to_not be_nil
    expect(f.id).to eql @uploaded_file.id
  end

  it 'should update a file' do
    n = 'new file name'
    @uploaded_file.update({ :name => n })
    expect(@uploaded_file.display_name).to eql n
  end

  it 'should delete a file' do
    id = @uploaded_file.id
    expect(@uploaded_file.destroy!).to be_truthy
    expect { CanvasFile.find(id) }.to raise_error(CanvasToolkit::CanvasClientError)
  end
end
