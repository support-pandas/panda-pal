require './spec/spec_helper'

describe Enrollment do

  describe 'enrollment creation' do
    before :each do
      course
      user
    end

    it 'should include a new status when created locally' do
      e = Enrollment.create(@user, @course)
      expect(e.additional_attributes[:status]).to eql 'new'
    end

    it 'should be pushable to canvas' do
      e = Enrollment.create!(@user, @course, nil, { :type => 'StudentEnrollment' })
      expect(e.id).to_not be_nil
    end
  end

  describe 'existing enrollment actions' do
    before :each do
      course_with_user
    end

    it 'should be able to be found' do
      e = Enrollment.find(@enrollment.root_account_id, @enrollment.id)
      expect(e.id).to eql @enrollment.id
    end

    it 'should allow the enrollment to be concluded' do
      account, id = @enrollment.root_account_id, @enrollment.id
      expect(@enrollment.conclude).to be_truthy
      @enrollment = Enrollment.find(account, id)
      expect(@enrollment).to_not be_nil
      expect(@enrollment.enrollment_state).to eql 'completed'
    end

    it 'should allow the enrollment to be deleted' do
      account, id = @enrollment.root_account_id, @enrollment.id
      expect(@enrollment.destroy!).to be_truthy
      @enrollment = Enrollment.find(account, id)
      expect(@enrollment).to_not be_nil
      expect(@enrollment.enrollment_state).to eql 'deleted'
    end
  end
end