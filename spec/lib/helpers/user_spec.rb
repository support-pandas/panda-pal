require './spec/spec_helper'

describe User do

  describe 'user creation actions' do
    it 'should include a new status when creating a local user' do
      c = User.create
      expect(c.additional_attributes[:status]).to eql 'new'
    end

    it 'should be pushable to canvas and retain attributes' do
      n = 'user name'
      a = User.create!({ :unique_id => SecureRandom.uuid, :name => n })
      expect(a.id).to_not be_nil
      expect(a.name).to eql n
      expect(a.login_id).to_not be_nil
    end
  end

  describe 'existing user actions' do
    before :each do
      user
    end

    it 'should allow attributes to be updated' do
      n = 'new user name'
      @user.update({ :name => n })
      expect(@user.name).to eql n
    end

    it 'should allow users to be found' do
      u = User.find @user.id
      expect(u).to_not be_nil
      expect(u.id).to eql @user.id
    end

    it 'should allow users to be merged' do
      dest_user = User.create!({ :unique_id => SecureRandom.uuid })
      id = dest_user.id
      source_login = @user.login_id
      @user.merge dest_user
      expect(@user.login_id).to be_nil
    end
  end

  describe 'user enrollments' do
    before :all do
      course_with_student({ :enrollment_state => 'active' })
    end

    it 'should return an array of enrollments for a given user' do
      expect(@user.enrollments('state[]=current_and_future').length).to eql 1
    end
  end

  describe 'file related actions' do
    before :all do
      user
      masq @user.id
      tmp_file
    end

    after :all do
      stop_masq
    end

    it 'should upload a file to a user' do
      f = @user.upload_file(@file)
      expect(f.id).to_not be_nil
    end

    it 'should return an array of files in a user' do
      f = @user.upload_file(@file)
      expect(@user.files).to be_a(Batch)
      expect(@user.files.length).to be_between(1, 100)
    end
  end

  describe 'folder related actions' do
    before :all do
      user
      masq @user.id
    end

    after :all do
      stop_masq
    end

    it 'should create a folder' do
      f = @user.create_folder!
      expect(f).to_not be_nil
      expect(f.id).to_not be_nil
    end

    it 'should find a folder' do
      f = @user.create_folder!
      nf = @user.find_folder f.id
      expect(nf).to_not be_nil
      expect(nf.id).to eql f.id
    end
  end
end