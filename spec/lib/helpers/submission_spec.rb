require './spec/spec_helper'

describe Submission do
  before :all do
    course_with_assignment({}, { :submission_types => 'online_upload', :published => true })
    @course.publish!
    user
    @course.enroll_student!(@user, { :enrollment_state => 'active' })
    masq @user.id
    tmp_file
    f = @assignment.upload_submission_file @user, @file
    @submission = @assignment.create_submission! submission_type: :online_upload, file_ids: f.id
    stop_masq
  end

  it 'should download a submission' do
    dl = @submission.download
    expect(dl).to be_truthy
  end

  it 'should grade a submission' do
    @submission.posted_grade = 5
    @submission.update
    expect(@submission.grade.to_i).to eql 5
  end
end