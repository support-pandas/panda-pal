require './spec/spec_helper'

describe AssignmentGroup do
  before :all do
    course_with_assignment
  end

  it 'should find an assignment group' do
    ag = AssignmentGroup.find(@course.id, @assignment.assignment_group_id)
    expect(ag).to_not be_nil
    expect(ag.id).to eql @assignment.assignment_group_id
  end
end