require './spec/spec_helper'

describe Assignment do
  before :all do
    course
  end

  describe 'assignment creation' do
    it 'should include a new status when creating a local assignment' do
      a = Assignment.create @course.id
      expect(a.additional_attributes[:status]).to eql 'new'
    end

    it 'should be pushable to canvas and retain attributes' do
      n = 'assignment name'
      a = Assignment.create!(@course.id, {:name => n})
      expect(a.id).to_not be_nil
      expect(a.name).to eql n
    end
  end

  describe 'existing assignments' do
    before :each do
      assignment({ :name => 'assignment name', :submission_types => 'online_text_entry' })
    end

    it 'should be findable' do
      a = Assignment.find @course.id, @assignment.id
      expect(a).to_not be_nil
      expect(a.id).to eql @assignment.id
    end

    it 'should be able to be updated' do
      n = 'a new name'
      p = 5
      @assignment.update({ :name => n, :points_possible => p })
      expect(@assignment.name).to eql n
      expect(@assignment.points_possible).to eql p
    end

    it 'should be able to be deleted' do
      id = @assignment.id
      expect(@assignment.destroy!).to be_truthy
      expect { Assignment.find @course, id }.to raise_error(CanvasToolkit::CanvasClientError)
    end

  end

  describe 'submissions' do
    before :all do
      user
      assignment({ :published => true, :name => 'assignment name', :submission_types => 'online_text_entry' })
      @course.publish!
      @course.enroll_student!(@user, { :enrollment_state => 'active' })
    end

    it 'should create a submission' do
      masq @user.id
      @submission = @assignment.create_submission!({ :submission_type => 'online_text_entry', :body => 'test' })
      expect(@submission).to_not be_nil
      expect(@submission.id).to_not be_nil
      stop_masq
    end

    it 'should return an array of submissions' do
      submissions = @assignment.submissions
      expect(submissions.length).to be_between(1, 1000)
    end

    it 'should find a submission' do
      s = @assignment.submissions.first
      expect(s).to_not be_nil
      expect(s.id).to_not be_nil
    end

    describe 'file based submissions' do
      before :all do
        @assignment.submission_types = 'online_upload'
        @assignment.update
      end

      after :all do
        stop_masq
      end

      it 'should upload a submission file' do
        masq @user.id
        tmp_file
        f = @assignment.upload_submission_file @user, @file
        expect(f).to_not be_nil
        expect(f.id).to_not be_nil

        sub = @assignment.create_submission! submission_type: :online_upload, file_ids: f.id
        expect(sub).to_not be_nil
        expect(sub.id).to_not be_nil
      end
    end
  end

end
