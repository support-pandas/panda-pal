require './spec/spec_helper'

describe Quiz do
  before :all do
    course
  end

  describe 'quiz creation' do
    it 'should include a new status when creating a local quiz' do
      a = Quiz.new @course.id
      expect(a.additional_attributes[:status]).to eql 'new'
    end

    it 'should be pushable to canvas and retain attributes' do
      n = 'quiz name'
      a = Quiz.create!(@course.id, {:title => n})
      expect(a.id).to_not be_nil
      expect(a.title).to eql n
    end
  end

  describe 'existing quizzes' do
    before :each do
      quiz({ :title => 'quiz name' })
    end

    it 'should be findable' do
      a = Quiz.find @course.id, @quiz.id
      expect(a).to_not be_nil
      expect(a.id).to eql @quiz.id
    end

    it 'should be able to be updated' do
      n = 'a new name'
      @quiz.update({ :title => n })
      expect(@quiz.title).to eql n
    end

    it 'should be able to be deleted' do
      id = @quiz.id
      expect(@quiz.destroy!).to be_truthy
    end
  end
end