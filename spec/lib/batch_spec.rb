require './spec/spec_helper'

describe Batch do
  before :all do
    course
  end

  it 'should update all items in a batch' do
    attr = { :name => 'Assignment name' }
    assignments = Batch.new [Assignment.create!(@course, attr), Assignment.create!(@course, attr)]
    ids = assignments.collect { |a| a.id }
    assignments.destroy!
    ids.collect { |i|
      expect { Assignment.find(@course, i) }.to raise_error(CanvasToolkit::CanvasClientError)
    }
  end

  it 'should return a Batch when Array methods are called' do
    arr = [1, 2, 3, 4, 5]
    batch = Batch.new arr
    batches = []

    *push_arg = 6, 7, 8, 9, 10

    batches <<
      batch_push  = batch.push(*push_arg) <<
      batch_map   = batch.map { |i| i * 2 } <<
      batch_slice = batch[0..6]

    batches.each do |b|
      expect(b.is_a? Batch).to be_truthy
      expect(b.is_a? Array).to be_falsey
    end
  end

end
