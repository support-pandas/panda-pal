require './spec/spec_helper'

describe Publishable do
  before :all do
    course_with_assignment
  end

  it 'should stage a publish event' do
    @assignment.publish
    expect(@assignment.staged['published']).to be_truthy
  end

  it 'should stage an unpublish event' do
    @assignment.unpublish
    expect(@assignment.staged['published']).to be_falsey
  end

  it 'should push a publish event' do
    @assignment.publish!
    expect(@assignment.staged['published']).to be_nil
    expect(@assignment.published).to be_truthy
  end

  it 'should push an unpublish event' do
    @assignment.unpublish!
    expect(@assignment.staged['published']).to be_nil
    expect(@assignment.published).to be_falsey
  end
end