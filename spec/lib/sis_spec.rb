require './spec/spec_helper'

describe SIS do

  describe 'CSV Error Handling' do
    context 'on creation' do
      it 'should raise an error when invalid header data is provided' do

      end

      it 'should raise an error when invalid body data is provided' do

      end

      it 'should raise an error when an invalid hash is provided' do

      end

      it 'should raise an error when invalid type is passed' do
        type = 'bananas'
        expect { SIS::CSV.new type }.to raise_error
      end
    end
  end

  describe 'CSV creation' do

    before :each do
      @enrollments = csv 'enrollment'
      @users = csv
      @fields = lambda { |type| Pref.csv[type][:fields] }
    end

    it 'should depluralize, downcase, and cast type to symbol' do
      type = 'eNROLLMENTS'
      expect(csv(type).type).to eql :enrollment

      csv_each { |type|
        type1 = type.to_s
        type1.upcase!
        type1 << 's'
        expect(csv(type1).type).to eql type
      }
    end

    it 'should be possible to pass string or symbol types' do
      sym = :users
      str = 'user'

      csym = csv sym
      cstr = csv str
      expect(cstr.type).to eql csym.type
      expect(cstr.type).to eql :user
    end

    it 'should compose a table of CSV data when a body and a list of optional fields are provided' do
      type = 'course'
      opt_fields = [:account_id, :start_date, :end_date]
      body = [
        'C01,A short name,A long name for a course,1,active,2014-12-30T00:00:00Z,2015-12-30T00:00:00Z',
        'C02,Another short name,Another long name,1,active,2014-01-01T00:00:00Z,2015-01-01T00:00:00Z'
      ]
      expected_header = [:course_id, :short_name, :long_name, :account_id, :status, :start_date, :end_date]
      expected_header_s = expected_header.join(',')

      c = csv type, body: body, include: opt_fields

      expect(c.header).to eql(expected_header)

      table = [] << expected_header_s
      body.each { |row|
        table << row
      }

      expect(c.table).to eql(table)
    end

    it 'should accept hash-style body data. Header should update accordingly' do
      body = {
        user_id: 'SIS user 007',
        login_id: 'bond007',
        status: 'active',
        last_name: 'Bond',
        full_name: 'James Bond'
      }

      header = @fields.call(:user).select { |f|
        body.key? f
      }

      csv 'user', body: body

      joined_body = header.map { |f|
        body[f]
      }

      #expect(@csv.header).to eql header
      #expect(@csv.body).to eql joined_body
    end

    it 'should support all SIS CSV types listed in API docs' do

    end
  end

  describe 'CSV manipulation' do
    it 'should support selective addition and subtraction of rows' do

    end

    it 'should be possible to query data and remove rows containing matches' do

    end

    it 'should be possible to make batch changes based on queries' do

    end
  end

  describe 'CSV generation' do
    it 'should generate sequential data' do

    end

    it 'should iterate on iterable fields only' do

    end

    it 'should accept a count of iteration and a starting point' do

    end


  end

  describe 'SIS File IO actions' do
    it 'should read existing CSV files and import into a CSV object' do

    end

    it 'should write a CSV object to a file' do

    end

    it 'should zip multiple existing files after reading' do

    end

    it 'should zip multiple files after writing' do

    end
  end

  describe 'SIS Importing' do
    it 'should upload a single CSV object to account' do

    end

    it 'should upload multiple CSV objects to account' do

    end

    it 'should read a CSV object from file, then upload to account' do

    end

    it 'should read multiple CSV objects from multiple files, then upload' do

    end

    it 'should read multiple CSV objects from a zip, then upload' do

    end
  end

  describe 'SIS Exporting' do
    it 'should export SIS data from account, and create CSV objects for each type exported' do

    end
  end

end
