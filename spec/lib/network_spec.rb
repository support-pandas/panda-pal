require './spec/spec_helper'

describe 'HTTP Requests' do
  before :each do
    @tmp_token = Pref.config[:token]
  end
  it 'should catch invalid tokens' do
    Pref.config[:token] = 'wow     such token     much fake'
    expect { Course.find(1) }.to raise_error(CanvasToolkit::CanvasClientError)
  end

  it 'should catch timeouts' do
    #TODO: think of a way to guarantee a gateway timeout using call/call_raw
  end

  after :each do
    Pref.config[:token] = @tmp_token
  end

end

describe 'File uploads' do
  before :all do
    course
  end

  before :each do
    tmp_file
  end

  it 'should upload a file' do
    resp, code = Network::call('/courses/' + @course.id.to_s + '/files', 'POST', { :file => @file }, true)
    expect(code).to eql 200
    expect(resp['id']).to_not be_nil
    expect(resp['display_name']).to eql File.basename(@file)
  end
end

describe 'Domain handling' do
  after (:all) do
    use_default_test_environment true
  end

  it 'should properly interpret short domains' do
    Network::set_domain 'test'
    expect(domain).to eql 'test.instructure.com'
  end

  it 'should properly interpret full domains' do
    Network::set_domain 'test.instructure.com'
    expect(domain).to eql 'test.instructure.com'
  end

  it 'should properly interpret full domains if prepended by a scheme' do
    Network::set_domain 'https://test.instructure.com'
    expect(domain).to eql 'test.instructure.com'
  end

  it 'should allow local installation domains to succeed' do
    Network::set_domain 'localhost:3000'
    expect(domain).to eql 'localhost:3000'
  end

  it 'should allow local installation domains to succeed if prepended with a scheme' do
    Network::set_domain 'http://localhost:3000'
    expect(domain).to eql 'localhost:3000'
  end

  it 'should extract a domain from a full API URL' do
    Network::set_domain 'https://test.instructure.com/api/v1/courses'
    expect(domain).to eql 'test.instructure.com'
  end

  it 'should extract a domain from a full API URL pointed at a local installation' do
    Network::set_domain 'http://localhost:3000/api/v1/courses'
    expect(domain).to eql 'localhost:3000'
  end

  it 'should properly interpret a domain if using a vanity URL' do
    Network::set_domain 'myelms.umd.edu'
    expect(domain).to eql 'myelms.umd.edu'
  end

  it 'should not allow schemes with vanity URLs (cannot accurately detect these)' do
    Network::set_domain 'test'
    Network::set_domain 'https://myelms.umd.edu'
    expect(domain).to_not eql 'myelms.umd.edu'
  end

  describe 'Domain construction' do
    before (:each) do
      Network::set_domain 'test'
      @pref_uri = (Pref.config[:scheme] ? Pref.config[:scheme] : 'https') + '://' + Pref.config[:domain] + '/api/v1/courses'
      @local_uri = 'https://localhost:3000/api/v1/courses'
    end

    it 'should throw an error if a domain is not set' do
      Pref.config[:domain] = nil
      expect { Network::add_domain('/courses') }.to raise_error(CanvasToolkit::DomainError)
    end

    it 'should not allow /api/v1 to be included if using shortcutting' do
      expect { Network::add_domain('/api/v1/courses') }.to raise_error
    end

    it 'should use the scheme specified by the configuration' do
      expect(Network::add_domain('/courses')).to eql @pref_uri
    end

    it 'should produce a fully qualified URL with no shortcutting' do
      expect(Network::add_domain(@local_uri)).to eql @local_uri
    end

    it 'should produce a fully qualified URL when using shortcutting' do
      expect(Network::add_domain('/courses')).to eql @pref_uri
    end
  end
end
