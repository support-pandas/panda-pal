require './lib/canvas_toolkit'

use_default_test_env unless Pref.config[:environment].eql? Pref.config[:default_test_environment]
Progress.suppress 'all'

def course(attr = {})
  @course = Course.create! attr
end

def course_with_assignment(course_attr = {}, assignment_attr = {})
  course course_attr
  assignment assignment_attr
end

def assignment(assignment_attr = {})
  assignment_attr[:name] ||= SecureRandom.uuid
  @assignment = @course.create_assignment!(assignment_attr)
end

def quiz(attr = {})
  attr[:title] ||= SecureRandom.uuid
  @quiz = @course.create_quiz!(attr)
end

def user(attr = {})
  attr[:unique_id] ||= SecureRandom.uuid
  @user = User.create!(attr)
end

def course_with_user(type = 'StudentEnrollment', attr = {}, course_attr = {}, user_attr = {})
  course(course_attr)
  user(user_attr)
  @enrollment = Enrollment.create!(@user, @course, nil, attr.merge({ :type => type }))
end

def course_with_student(attr = {})
  course_with_user('StudentEnrollment', attr)
  @student = @user
end

def course_with_teacher(attr = {})
  course_with_user('TeacherEnrollment', attr)
  @teacher = @user
end

def tmp_file(attr = {})
  content = attr[:content] ? attr[:content] : SecureRandom.hex
  f = File.new('./tmp/' + SecureRandom.uuid, 'w')
  f.write content
  f.close
  @file = f.path
end

def section(attr = {})
  attr[:name] ||= SecureRandom.uuid
  @section = @course.create_section!(attr)
end

def csv(type = 'user', args = {})
  @csv = SIS::CSV.new type, args
end

def csv_each
  Pref.csv.each do |type, opts|
    required = opts[:required]
    fields = opts[:fields]
    optional = fields.reject { |f| required.include? f }
    yield type, fields, required, optional
  end
end
