require 'yaml'
require 'yaml/store'
require 'colorize'
require './lib/misc'

def handle_file(name)
  raise 'A block must be given' unless block_given?
  config_file = YAML.load_file name
  raise 'Missing file %s' % name unless config_file
  yield config_file
end

def input(m)
  puts m
  putc '>'
end

def add_env(env = 'prod')
  @config['environments'][env] ||= {}

  input 'Enter a default domain (such as canvas.instructure.com):'
  @config['environments'][env]['domain'] = gets.chomp.downcase
  puts ('Using domain: ' << @config['environments'][env]['domain']).green

  input 'Enter an API token for use with this toolkit:'
  @config['environments'][env]['token'] = gets.chomp
  puts 'Using given token'.green

  input 'Do you want safe mode to be enabled? [yes]'
  res = gets.chomp.downcase
  @config['environments'][env]['safe_mode'] = res.empty? || Misc::TRUTHY.include?(res)
  puts !!@config['environments'][env]['safe_mode'] ? 'Safe mode enabled'.green : 'Safe mode disabled'.red

  input 'Input default http scheme [https]'
  res = gets.chomp.downcase
  @config['environments'][env]['scheme'] = res.empty? ? 'https' : res
  puts ('Using http scheme: ' << @config['environments'][env]['scheme']).green
end

handle_file('./etc/config.yml') do |config_file|
  @config = config_file
  @config['environments'].select! { |k, v| k == 'prod' }

  add_env

  loop do
    input 'Do you want to add another environment? [N]'
    res = gets.chomp.downcase
    break if res.empty? || Misc::FALSEY.include?(res)

    input 'What name would you like to give to this new environment?'
    add_env gets.chomp.downcase
  end

  puts 'Environments:'.blue
  @config['environments'].keys.each { |e| puts e }
  puts ''
  input 'Which environment should be the default? [prod]'
  res = gets.chomp.downcase
  @config['default_environment'] = res.empty? ? 'prod' : res
  puts ('Setting default environment to ' << @config['default_environment']).green

  store = YAML::Store.new './etc/config.yml'
  store.transaction do
    @config.each do |k, v|
      store[k] = v
    end
  end

  puts 'Enjoy the toolkit!'.blue
  puts 'Please report any bugs that you may encounter'.blue
end