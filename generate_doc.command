#!/bin/bash
cd `dirname $0`
rm -r doc
rdoc --markup markdown --include './lib/' --exclude 'tmp/*' --exclude 'doc/*' --exclude 'etc/*' --exclude 'script/*' --exclude 'spec/*' --visibility=public