#!/bin/bash
cd `dirname $0`
rvm install jruby
rvm use jruby
gem install bundler
bundle install --without ruby
