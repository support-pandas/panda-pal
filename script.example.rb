require './utils'

# Write out your script here!
puts call 'https://byoung.instructure.com/api/v1/accounts/1/courses', 'GET'

# You can also perform domain shortcutting if you have used a fully qualified domain once
# in the script or IRB session, or explicity define it using set_domain(domain)

# Explicitly sets a domain
set_domain 'canvas.instructure.com'

# Implicitly sets a domain based on the uri
call 'https://byoung.instructure.com/api/v1/enrollments'

# Performing another call like this will use the same domain as the previous call
# (will throw a runtime error if called without having a domain set either implicitly or explicitly)
# Don't include /api/v1 if using shortcutting
# Remember to include the initial forward slash /
#
# Based on the previous example call, this will use the uri 'https://byoung.instructure.com/api/v1/courses'
call '/courses'