#!/bin/bash
cd `dirname $0`
rvm install ruby
rvm --default use ruby
gem install bundler
bundle install --without development
