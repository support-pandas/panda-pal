module Publishable

  # #### Sets an object as published, but does not push the update to Canvas.
  def publish
    self.published = true
  end

  # #### Sets an object as published, and pushes the update to Canvas.
  #
  # **Returns**
  # - true if the update succeeds, false otherwise
  def publish!
    publish
    update
  end

  # #### Sets an object as unpublished, but does not push the update to Canvas.
  def unpublish
    self.published = false
  end

  # #### Sets an object as unpublished, and pushes the update to Canvas.
  #
  # **Returns**
  # - true if the update succeeds, false otherwise
  def unpublish!
    unpublish
    update
  end

  # #### Checks if the object is published
  #
  # **Returns**
  # - true if the object is published, false otherwise
  def published?
    !!self.published
  end

  # #### Checks if the object is unpublished
  #
  # **Returns**
  # - true if the object is unpublished, false otherwise
  def unpublished?
    !published?
  end

  # #### Checks if the object is unpublishable
  #
  # **Returns**
  # - true if the object is unpublishable, false otherwise
  def unpublishable?
    !!self.unpublishable
  end

end