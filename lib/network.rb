require 'net/http'
require 'json'
require 'openssl'
require 'fileutils'
require 'date'
require 'rest_client'
require 'colorize'
require './lib/canvas_toolkit'
require './lib/progress'

module Network

  @@count ||= 0

  def self.call_raw(uri, method, opts = nil, use_token = true)
    uri = URI(uri) unless uri.is_a?(URI)
    @@count += 1
    Net::HTTP.start(uri.host, uri.port, :use_ssl => uri.scheme == 'https',
                    :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http|
      case method
      when 'POST'
        request = Net::HTTP::Post.new uri.request_uri
      when 'PUT'
        request = Net::HTTP::Put.new uri.request_uri
      when 'DELETE'
        request = Net::HTTP::Delete.new uri.request_uri
      else
        request = Net::HTTP::Get.new uri.request_uri
      end

      request.initialize_http_header({
        'Authorization' => 'Bearer ' + Pref.config[:token]
      }) if use_token

      if not method == 'GET'
        request.set_form_data opts unless opts.nil?
      end

      response = http.request request
      CanvasToolkit::handle_canvas_errors response

      body = response.body
      return body, response.code.to_i
    end
  end

  def self.call(uri, method = 'GET', opts = nil, file_upload = false)
    raise DomainError unless domain
    method.upcase!
    unless uri.is_a? URI
      set_domain(uri)
      uri = add_domain(uri)

      if masquerading?
        if method == 'GET'
          if uri.match(/\?/)
            uri += '&as_user_id=' + masquerading_as.to_s
          else
            uri += '?as_user_id=' + masquerading_as.to_s
          end
        else
          opts ||= {}
          opts[:as_user_id] = masquerading_as.to_s
        end
      end
      uri = URI(uri)
    end
    body, code = file_upload ? file_upload(uri, opts) : call_raw(uri, method, opts)
    return JSON.parse(body), code
  end

  def self.file_upload(uri, opts = nil)
    raise ArgumentError, 'File not specified' if opts && !opts.include?(:file)
    file = opts[:file].is_a?(File) ? opts[:file] : File.new(opts[:file], 'rb')
    opts.delete(:file)
    opts[:name] ||= File.basename file.path
    res, code = call(uri, 'POST', opts)
    return res, code unless code == 200
    mas = masquerading_as
    RestClient.post(res['upload_url'], res['upload_params'].merge({ res['file_param'] => file })) do |response, request, result, &block|
      if response.code == 200
        masquerade mas
        return response.body, response.code
      elsif [301, 302, 307].include?(response.code)
        loc = response.headers[:location]
        begin
          response.follow_redirection(request, result, &block)
        rescue
          return call_raw(loc, 'GET', nil, false)
        end
      else
        response.return!(request, result, &block)
      end
    end
  end

  def self.file_download(uri, name = SecureRandom.uuid, file_store = './tmp', opts = nil)
    file_store = file_store << '/' unless file_store.end_with?('/')
    data = nil
    RestClient.get(uri) do |response, request, result, &block|
      if response.code == 200
        data = response.body
      elsif [301, 302, 307].include?(response.code)
        loc = response.headers[:location]
        begin
          response.follow_redirection(request, result, &block)
        rescue
          data = call_raw(loc, 'GET', nil, false)
        end
      end
    end

    unless data.nil?
      Dir.mkdir(file_store) unless Dir.exists?(file_store)
      f = File.new(file_store << name, 'w')
      r = f.write data
      f.close
      return r > 0
    end
    !!data
  end

  def self.all_records(uri, attr = [], paginate = true)
    page = 1
    unless attr.is_a? Array
      attr = attr.split '&'
    end
    attr.push 'per_page=' + Pref.config[:per_page].to_s
    s = build_arguments_string attr
    a = []
    Progress.unknown_bar {
      loop do
        c, code = call(uri + s + '&page=' + page.to_s)
        if code < 300
          a = a + c
          page += 1
          Progress.current_count = a.length
          Progress.format 'count' => (a.length).to_s
        end
        if c.length < Pref.config[:per_page] || code >= 300 || !paginate
          break
        end
      end
    }
    a
  end

  def self.build_arguments_string(attr)
    s = '?'
    attr.each do |v|
      s += v + '&'
    end
    s[0, s.length - 1]
  end

  # This method should only be used internally
  def self.add_domain(uri)
    if uri.match(/^\//)
      raise CanvasToolkit::DomainError unless Pref.config[:domain]
      raise ArgumentError, '/api/v1 should not be included when using shortcutting' if uri.match(/\/api\/v1/)
      return (Pref.config[:scheme] ? Pref.config[:scheme] : 'https') + '://' + Pref.config[:domain] + '/api/v1' + uri
    end
    uri
  end

  def self.set_domain(dom, only_return = false)
    dom = dom.to_s
    dom += '.instructure.com' if !dom.match(/\.|\/|:/)
    regexes = [
      /\/\/(.*?.*?\.com)/, # Capture url if scheme is included
      /(.*?.*?\.com)/, # Capture if scheme is not included
      #/\/\/(.*?:\d{3,5})/,
      #/(.*?:\d{3,5})/, # local installations
      /(^[^\/]*$)/ # Last resort: catch anything that doesn't have forward slashes
    ]
    regexes.each do |regex|
      a = dom.match regex
      if a
        Pref.config[:domain] = a[1].to_s unless only_return
        break if a[1].to_s
      end
    end
    only_return ? dom : Pref.config[:domain]
  end

  # #### Retrieves, sets, or uses a domain temporarily.
  #
  # - If no arguments are provided, this simply returns the currently used domain.
  # - If a string is given (but no block), the string is set as the domain.
  # - If both a string and a block are given, the string is used as the domain for the duration of the block.  Afterwards, the domain returns to
  # the one used previously.
  #
  # **Arguments**
  #
  # - dom: the domain to be used, either set as the currently active domain, or used for the duration of a given block.
  # - &block: executes the given block using the temporary domain provided.
  #
  # **Returns**
  #
  # - returns the currently used domain if no arguments are given.
  def self.domain(dom = nil, &block)
    return Pref.config[:domain] unless dom
    if block_given?
      begin
        Pref.config[:temp_domain] = Pref.config[:domain]
        set_domain dom
        yield
      ensure
        set_domain Pref.config[:temp_domain]
      end
    else
      set_domain dom
    end
  end

  def self.counter
    @@count
  end
end

# Placing this outside of the module for ease of access from terminal
def domain(dom = nil, &block)
  Network::domain(dom, &block)
end
