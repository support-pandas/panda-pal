require 'ruby-progressbar'
module Progress

  BG_INC_SPEED = 0.5

  @current_bar = nil
  @current_style = nil
  @depth = 0
  @refresh_hooks = []

  def self.bar(opts = {}, &block)
    opts[:style] ||= :known
    if (Pref.config[:suppress_progress] & [:fixed, 'all']).empty? || ui_enabled?
      begin
        start(opts)
        block.call
      ensure
        finish
      end
    else
      block.call
    end
  end

  def self.unknown_bar(opts = {}, &block)
    opts[:style] ||= :unknown
    opts[:type] ||= :list

    if (Pref.config[:suppress_progress] & [opts[:type], 'all']).empty? || ui_enabled?
      begin
        start(opts)

        Thread.new {
          bar = Thread.new {
            loop do
              sleep BG_INC_SPEED
              Progress.inc
            end
          }

          block.call
          bar.exit
          Thread.exit
        }.join
      ensure
        finish
      end
    else
      block.call
    end
  end

  # #### Creates a progress bar and handles initial depth logic
  #
  # - Only use this if Progress::bar or Progress::unknown_bar won't work for some reason.
  # - Progress::finish _must_ be called, otherwise bars will not work correctly.
  #
  # **Accepts**
  #
  # - opts: hash of following options
  #
  #     - title: optional; string to include before progress bar
  #     - total: optional; int of progress bar total
  #
  # **Returns**
  #
  # - nil
  def self.start(opts = {})
    @depth += 1
    if @current_bar.nil?
      self.current_count = 0
      @depth = 0
      @current_style = opts[:style]
      bar_opts = {:format => format('count' => '0'),
                  :smoothing => 0.9,
                  :total => opts[:total] }
      bar_opts.merge!({ :output => (Misc::OS.windows? ? IO.pipe[1] : File.new('/dev/null', 'w')) }) if ui_enabled?

      @current_bar = ProgressBar.create(bar_opts)
      @current_bar.log opts[:title] if opts[:title]
      @refresh_hooks.each do |r|
        @current_bar.add_refresh_hook r
      end
    end
  end


  def self.inc(i = 1)
    @current_bar.progress += i if valid?
  end

  def self.dec(i = 1)
    @current_bar.progress -= i if valid?
  end

  def self.finish
    if valid?
      if ui_enabled?
        # Handle the final update from the bar manually here, as a regular stop will cause undesired threading behavior
        @current_bar.output.refresh force: true
      else
        @current_bar.stop
      end
      @current_bar = nil
      finish_hook.call if finish_hook
    else
      @depth -= 1
    end
  end

  def self.title(title)

  end

  def self.format(opts = {}, str = nil)
    if @current_style || str
      str ||= get_format @current_style
      match = str[/\#\!\w+/]
      if match
        hash_fodder = opts.map { |opt| ['#!' << opt[0], opt[1]] }
        result = str.gsub(match, Hash[hash_fodder])
        if @current_bar
          @current_bar.format result
        else
          result
        end
      else
        str
      end
    end
  end

  private

  def self.get_format(style)
    case(style)
    when :known
      '%a %E |%B| %p%%'
    when :unknown
      '%a Items found: #!count %B'
    end
  end

  def self.valid?
    @depth == 0 && !!@current_bar
  end

  def self.suppress(type, &block)
    if block_given?
      tmp = Pref.config[:suppress_progress].include? type
      Pref.config[:suppress_progress].push type
      block.call
      Pref.config[:suppress_progress].delete type if !tmp
    else
      if Pref.config[:suppress_progress].include? type
        Pref.config[:suppress_progress].delete type
      else
        Pref.config[:suppress_progress].push type
      end
    end
  end

  def self.suppressed?
    !Pref.config[:suppress_progress].empty?
  end

  def self.add_refresh_hook(pr = nil, &block)
    @refresh_hooks.push pr unless pr.nil?
    @refresh_hooks.push block if block_given?
  end

  def self.unknown?
    valid? && @current_style == :unknown
  end

  def self.known?
    !unknown?
  end

  class << self
    attr_accessor :finish_hook
    attr_accessor :current_count
    attr_accessor :title
  end
end
