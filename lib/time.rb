class Time

  def to_s
    utc.strftime('%Y-%m-%dT%H:%M:%SZ')
  end

end
