require './lib/network.rb'
require './lib/misc.rb'

module SIS
  # Uploads a CSV or an array of CSVs to an account
  #   if file path is provided instead of CSV object, read file and upload if valid
  #   if CSV object is provided, write to file if valid, then upload
  #   if array of objects or paths is provided
  #     if objects: write to file if valid
  #     if paths: read from files, check validity
  #     both: create zip from files, upload zip
  #   if zip is provided, unzip, check validity, re-zip, upload
  def import(account_id, csv)
    #TODO: show progress while zipping, uploading etc.

  end

  # Upload csv to account
  def upload_csv(account_id)

  end
  alias_method :save_csv, :upload_csv

  # Reads SIS data from a CSV file, returns a CSV object
  # considers CSV to conform to given type
  # if filename matches type (enrollments.csv), then type will be inferred
  def read_csv(file, type)

  end

  # TODO: this is a mess - WIP
  def write_csv(csv, filename = nil, dirname = 'etc/SIS Files')
    Dir.mkdir(dirname) unless File.directory?(dirname)
    csv = File.new filename, "w"
    return_vals = []
    count.times do |i|
      j = i
      j += params[:start_at].to_i if params[:start_at]

      csv.puts entry.call(j)
      return_vals.push entry.call(j).split(',')[return_pos] # default return_pos is 0
    end

    csv.close
  end

  class CSV
    attr_reader :type, :header, :header_s, :body, :table
    attr_accessor :filename, :start_at, :count

    class << self
      attr_accessor :start_at, :count
    end


    # type = type of SIS CSV
    # body = optional; array of formatted CSV data. Do not include header.
    #       Each array element is a row of CSV
    # opt  = optional; hash of options
    #     Options
    #       :start_at = integer at which to start iterating
    #       :count  = number of times to iterate
    #       :filename = alternate filename - defaults to %{type}s.csv (eg, users.csv)
    #       :include  = Array or comma separated list of optional (and optionally, required)
    #             fields. Order does not matter.
    def initialize(type, opt = {})
      @type = normalize_type type
      raise 'CSV type ' << @type.to_s << ' is invalid or not supported' unless Pref.csv.keys.include? @type
      @body     ||= opt[:body]     || []
      @start_at ||= opt[:start_at] ||= Pref.config[:sis][:start_at]
      @count    ||= opt[:count]    ||= Pref.config[:sis][:count]

      setup opt[:include] || [], opt[:filename]
      update_table
    end

    # add array of CSV formatted data to body and table
    def add(row)
      if row.is_a? Array
        row.each { |r|
          @body << r
        }
      else
        @body << row
      end

      update_table
    end

    # remove rows that match regex
    def remove(regex)
    end

    # Generates sequential data for CSV creation
    # All names, emails, and SIS IDs will have an iterator added to the end (Spam Student 0, Spam Student 1, etc.)
    # Specify ':start_at' to start at a specific number (start_at = 100: Spam Student 100, Spam Student 101, etc.)
    def self.generate(opt = {})
      @start_at ||= opt[:start_at] ||= Pref.config[:sis][:start_at]
      @count  ||= opt[:count]  ||= Pref.config[:sis][:count]

      puts @start_at
      puts @count

      if !opt[:course_id] && @type == :enrollment
        raise 'course_id must be specified for ' << @type << ' CSV'
      end

      opt[:email] ||= Pref.config[:sis][:email]
      opt[:email_domain] ||= Pref.config[:sis][:email_domain]
      if opt[:email]
        opt[:email], opt[:email_domain] = opt[:email].split('@')
        raise 'No alias specified in email - are you sure you want to do this?' if !(opt[:email] =~ /\+/) && Pref.csv[:safe_mode]
      end

      Pref.config[:sis].each { |k, v|
        opt[k] ||= v
      }

      iterable_fields = []

      data = []
      @count.times { |i|
        j = (i + @start_at).to_s
        cell = ''

        puts j
        @header.each_index { |m|
          h = @header[m]
          if h == :email
            cell << opt[:email] << j << '@' << opt[:email_domain]
          else
            cell << opt[h]
            cell << j if iterable_fields.include? h
          end
          cell << ',' unless m > @header.length - 2
        }

        data << cell
      }
      data
    end

    def generate(opt = {})
      SIS::CSV.generate opt
    end

    def generate!(opt = {})
      generate(opt)
      update_table
    end

    def self.help(type, verbose = Pref.config[:sis][:verbose_help])
      type = normalize_type type

      raise 'Unknown type ' << type.to_s unless Pref.csv[type]

      puts "\nAll fields for " << type.to_s << "s: "
      puts Pref.csv[type][:fields].join(', ')
      if verbose
        puts "\nRequired fields:"
        puts Pref.csv[type][:required].join(', ')
        puts "\nOptional fields:"
        puts Pref.csv[type][:fields].reject { |f| Pref.csv[type][:required].include? f }.join(', ')
      end
      puts "\n"

      true
    end


    private

    # Sets instance variables based on CSV type
    # opt_fields - array of optional fields to include
    # filename - optional; CSV filename
    def setup(opt_fields, filename = nil)
      filename ||= @type.to_s << "s.csv"
      opt_fields.map! { |f| f.to_sym }

      @header = Pref.csv[@type][:fields].select { |field|
        # only keep required or specified optional fields
        field_required?(field) || opt_fields.include?(field)
      }
      @header_s = @header.join(',')
      @filename = filename
    end

    # re-assembles @table from @header and @body
    def update_table
      @table = []
      @table << @header_s
      @body.each { |row|
        @table << row
      }
    end

    # Assumes all types end in 's'
    #   We'll need to find a better linguistic gem if this changes...
    def self.normalize_type(type)
      type = type.to_s if type.is_a? Symbol
      type.downcase!
      type = type[0..-2] if type[-1] == 's'
      type.to_sym
    end

    def normalize_type(type)
      SIS::CSV.normalize_type type
    end

    def field_required?(field)
      Pref.csv[@type][:required].include?(field)
    end
  end
end
