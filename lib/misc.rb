require 'active_support/inflector'
require 'rbconfig'

module Misc

  TRUTHY = [1, 't', 'true', 'y', 'yes']
  FALSEY = [0, 'f', 'false', 'n', 'no']

  class << self
    ActiveSupport::Inflector.inflections(:en) do |inflect|
      inflect.plural /^this$/, 'these'
      inflect.plural /^is$/, 'are'
    end
  end

  @safe_mode_messages = {
      file_upload: proc { |file| 'Are you sure you want to upload ' << file.to_s << '? (y/N)' },
      delete: proc { |what| 'Are you sure you want to delete ' << what.to_s << '? (y/N)' },
      merge: proc { 'Are you sure you want to merge these users?' }
  }

  # Pass a symbol to match the keys in @safe_mode_messages hash
  def self.check_safe_mode?(message = 'Are you sure you want to do this?', *args, &block)
    if block_given?
      if send_safe_mode_message?(message, *args)
        tmp = Pref.config[:safe_mode]
        Pref.config[:safe_mode] = false
        begin
          yield
        ensure
          Pref.config[:safe_mode] = tmp
        end
      end
    else
      send_safe_mode_message? message, *args
    end
  end

  def self.pluralize(str, len = 2)
    return str if len == 1
    str.pluralize
  end

  def self.pluralize_multi_word(str, len = 2, type = :rails)
    return '' if str.strip.empty?
    str = str.gsub(/([A-Z])/, ' \1').strip.downcase.split(/ /)
    (str[0, str.length - 1].join(' ') << ' ' << pluralize(str.last, len)).strip
  end

  private

  def self.send_safe_mode_message?(message, *args)
    return true if Pref.config[:safe_mode_suppressed]
    if Pref.config[:safe_mode]
      message = @safe_mode_messages[message].call(*args) if message.is_a? Symbol
      puts message
      resp = gets.chomp.downcase
      return Misc::TRUTHY.include? resp
    end
    true
  end

  module OS
    include RbConfig

    def OS.windows?
      !!CONFIG['host_os'].match(/mswin|windows/i)
    end

    def OS.mac?
      !!CONFIG['host_os'].match(/darwin/i)
    end

    def OS.unix?
      mac? || linux?
    end

    def OS.linux?
      !!CONFIG['host_os'].match(/linux|arch/i)
    end
  end
end
