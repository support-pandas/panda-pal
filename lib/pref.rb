require 'yaml'
require 'yaml/store'

module Pref

  class << self
    attr_accessor :config
    attr_accessor :attributes
    attr_accessor :csv
    attr_accessor :ui
  end

  def self.load(silent = false)

    # Config

    Pref.config = {}
    handle_file('./etc/config.yml') do |config_file|
      Pref.config[:per_page] = config_file['items_per_page'] if config_file['items_per_page']

      Pref.config[:environments] = {}
      config_file['environments'].each do |k, v|
        Pref.config[:environments][k.to_sym] = v
      end
      Pref.config[:default_environment] = config_file['default_environment'].to_sym
      Pref.config[:default_test_environment] = config_file['default_test_environment'].to_sym

      if Pref.config[:default_environment] && Pref.config[:environments][Pref.config[:default_environment]]
        Pref.config[:environment] = Pref.config[:default_environment]
        e = Pref.config[:environments][Pref.config[:default_environment]]
        if e['domain']
          Network::set_domain(e['domain'])
          puts 'Setting default domain: ' + domain unless silent
        end

        if e['token']
          Pref.config[:token] = e['token']
          puts 'Using included token' unless silent
        end

        unless e['safe_mode'].nil?
          Pref.config[:safe_mode] = e['safe_mode']
          puts 'Safe mode ' + (e['safe_mode'] ? 'enabled' : 'disabled') unless silent
        end

        Pref.config[:scheme] = e['scheme'] ? e['scheme'] : 'https'
        Pref.config[:default_progress_suppression] = config_file['default_progress_suppression'] || []
        Pref.config[:suppress_progress] = e['progress_suppression'] ? e['progress_suppression'] : Pref.config[:default_progress_suppression]
        Pref.config[:suppress_progress].map! { |a| a.to_sym }
      end

      if config_file['sis']
        Pref.config[:sis] = {}
        config_file['sis'].keys.each { |cfg|
          Pref.config[:sis][cfg.to_sym] = config_file['sis'][cfg]
        }
        puts 'SIS CSV Defaults loaded' unless silent
      end
    end

    # Attributes

    Pref.attributes = {}
    handle_file('./etc/attributes.yml') do |y|
      y.each do |k, v|
        Pref.attributes[k] = {}
        v.each do |k1, v1|
          Pref.attributes[k][k1] = { 'api_name' => v1[0], 'access' => v1[1] }
          Pref.attributes[k][k1].merge!({ 'required' => v1[2] }) unless v1.length < 3
        end
      end
    end
    HelperBase.load_attribute_methods

    # CSV

    Pref.csv = {}
    handle_file('./etc/csv.yml') do |y|
      y.each { |type, fields|
        type = type.to_sym
        Pref.csv[type] = {}
        Pref.csv[type][:required] = []
        Pref.csv[type][:fields] = fields.keys.map { |f| f.to_sym }
        fields.each { |field, required|
          Pref.csv[type][:required] << field.to_sym if required
        }
      }
    end
  end

  def self.load_ui_prefs
    if ui_enabled?
      # UI
      Pref.ui = {}
      FileUtils::cp('./etc/ui.yml.example', './etc/ui.yml') if !File.exist?('./etc/ui.yml') && File.exist?('./etc/ui.yml.example')
      handle_file('./etc/ui.yml') do |y|
        java_import javax.swing.UIManager
        Pref.ui[:default_lf] = (y['default_lf'] || UIManager.get_system_look_and_feel_class_name)
        Pref.ui[:frame_dimensions] = y['frame_dimensions']
      end
    end
  end

  def self.handle_file(name)
    raise 'A block must be given' unless block_given?
    config_file = YAML.load_file name
    raise 'Missing file %s' % name unless config_file
    yield config_file
  end

  def self.save
    config_hash = {}
    config_hash[:environments] = Pref.config[:environments]
    config_hash[:default_environment] = Pref.config[:default_environment]
    config_hash[:default_test_environment] = Pref.config[:default_test_environment]
    config_hash[:items_per_page] = Pref.config[:per_page]
    config_hash[:sis] = Pref.config[:sis]
    save_with_hash('config.yml', config_hash)
    save_with_hash('ui.yml', Pref.ui)
    load(true)

    # This should only used if the ui is enabled, and all components within the frame are validated
    UI::frame.refresh_all_views if ui_enabled? && UI::ui_loaded?
  end

  private
  def self.save_with_hash(file_name, hash)
    YAML::Store.new(File.expand_path('./etc/' << file_name, Dir.pwd)).transaction do |y|
      hash.each do |k, v|
        y[k.to_s] = v
      end
    end
  end
end
