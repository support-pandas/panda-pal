module CanvasToolkit
  Error = Class.new StandardError

  class ConfigurationError < Error; end

  # Thrown when a domain is not set
  class DomainError < Error
    def message; 'A domain must be set. Use "domain <[your_domain].instructure.com>" to set a domain.'; end
  end

  # Indicates an issue with an object's URIs
  class RoutingError < Error; end

  class CanvasError < Error
    def initialize(response_body, *args)
      response = JSON.parse response_body
      @error_report = "#{Pref.config[:scheme]}://#{domain}/error_reports/" << response['error_report_id'].to_s if response['error_report_id']
      @message = response['message']
      @errors = response['errors']
      super(*args)
    end

    def message
      report = @error_report.to_s if @error_report
      #messages = @messages.map { |h| h['message'] }.join(' ')

      message = "#{@messages.to_s}"
      message << "\n\tError Report: #{report}" if report
    end
  end

  class CanvasClientError < CanvasError; end
  class CanvasServerError < CanvasError; end

  def self.handle_canvas_errors(response)
    code = response.code.to_i
    body = response.body

    if code >= 400 && code < 500
      raise CanvasClientError, body, caller
    elsif code >= 500
      raise CanvasServerError, body, caller
    end
  end
end
