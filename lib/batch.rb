require './lib/helpers/helper_base'
require 'active_support/inflector'
require './lib/progress'

# Methods in this class shouldn't be accessed directly, as they are used to manipulate an array of objects already.
class Batch

  def initialize(arr = [])
    @data = arr
  end

  def method_missing(m, *args, &block)
    return handle_array_method(m, *args, &block) if @data.class.instance_methods.include? m
    to_remove = []
    Progress.bar(total: @data.length) {
      @data.each do |e|
        class_name = e.class.name
        if Pref.config[:safe_mode] && !Pref.config[:safe_mode_suppressed] && SAFE_MODE_METHODS.include?(m)
          Pref.config[:safe_mode_suppressed] = Misc::check_safe_mode?('Are you sure you want to ' <<
            m.to_s << ' ' <<
            Misc.pluralize('this', @data.length) << ' ' <<
            @data.length.to_s << ' ' <<
            Misc.pluralize_multi_word(class_name, @data.length) << '?')

          break if !Pref.config[:safe_mode_suppressed]
        end

        status = e.__send__(m, *args, &block)
        Progress.inc
        to_remove.push(e) if Misc::TRUTHY.include?(status.to_s) && m == :destroy
      end
      to_remove.each { |d| @data.delete d }
      to_remove.clear
    }
  ensure
    Pref.config[:safe_mode_suppressed] = false
  end

  def handle_array_method(m, *args, &block)
    tmp = nil

    if block_given?
      # Increment bar on each block iteration
      block_hook = proc { |*bargs|
        Progress.inc
        block.call(*bargs)
      }

      Progress.bar(total: @data.length) {
        tmp = @data.method(m).call(*args, &block_hook)
      }
    else
      tmp = @data.method(m).call(*args)
    end

    if tmp.is_a?(Array) || tmp.is_a?(Enumerator)
      Batch.new tmp.to_a
    else
      tmp
    end
  end

  def to_a
    @data
  end
  alias :to_ary :to_a

  SAFE_MODE_METHODS = [:destroy, :merge]
end
