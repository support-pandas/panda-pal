require 'securerandom'

# Require bundler/setup, so git gems are loaded into memory
require 'bundler/setup'
require 'active_support/core_ext/hash'

module CanvasToolkit
  # Loads up all modules
  @depends = [
    '/network.rb',
    '/sis.rb',
    '/pref.rb',
    '/misc.rb',
    '/batch.rb',
    '/time.rb',
    '/errors.rb',
    '/helpers/course.rb',
    '/helpers/assignment.rb',
    '/helpers/user.rb',
    '/helpers/conversations.rb',
    '/helpers/account.rb',
    '/helpers/enrollment.rb',
    '/helpers/file.rb',
    '/helpers/folder.rb',
    '/helpers/section.rb',
    '/helpers/quiz.rb',
    '/helpers/assignment_group.rb',
    '/helpers/group.rb',
    '/helpers/submission.rb',
    '/helpers/discussion_topic.rb',
    '/helpers/discussion_topic_entry.rb',
    '/helpers/group_discussion_topic.rb',
    '/helpers/group_discussion_topic_entry.rb',
    '/helpers/course_discussion_topic.rb',
    '/helpers/course_discussion_topic_entry.rb',
    '/helpers/communication_channel.rb',
    '/helpers/notification_preference.rb',
    '/helpers/page_view.rb',
    '/helpers/page.rb',
    '/helpers/course_page.rb',
    '/helpers/group_page.rb',
    '/exceptions/method_not_found.rb',
    '/mixins/publishable.rb',
    '/../script/script_loader.rb'
  ]
  @pwd = File.dirname(__FILE__)
  @@loaded ||= false

  def self.update_prompt
    if defined?(Pry)
      Pry.config.prompt = proc do |obj, nest, _|
        str = "Env: #{Pref.config[:environment]};".blue
        str << " Masq: #{masq_as};".green if masq?
        str << " [#{_.input_array.size.to_s}] #{obj}> "
        end
    end
  end

  def self.load_toolkit
    @@loaded = true
    @depends.each { |d|
        require @pwd + d
    }

    Pref.load
    CanvasToolkit::update_prompt
    raise 'Token not specified in etc/config.yml' unless Pref.config[:token]

    # Initialize the script loader here, so it can be accessed in an IRB session
    ScriptLoader::load

    # If this argument is added (such as being run from scripts.command), load the script selector immediately
    argv = ARGV.shift
    if argv == '--script-loader'
      ScriptLoader::selector
    elsif argv == '--ui'
      raise ConfigurationError.new('The --ui flag can only be used with JRuby') unless RUBY_ENGINE == 'jruby'

      # require UI module only when using jruby and the ui flag is set.  This would raise a runtime error otherwise
      require "#{File.dirname(__FILE__)}/ui/ui.rb"
      UI::initialize
    end
  end
  load_toolkit unless !!@@loaded

  def self.reload
    @depends.reject! { |d|
      d =~ /misc/
    }
    @depends.each { |d|
      load @pwd + d
    }
  end
  load_toolkit unless !!@@loaded

  def self.reload
    @depends.reject! { |d|
      d =~ /misc/
    }
    @depends.each { |d|
      load @pwd + d
    }
  end
end

def reload
  puts 'Reloading...'
  CanvasToolkit.reload
end

def set_environment(env, silent = false)
  if Pref.config[:environments][env]
    Pref.config[:environment] = env.to_sym
    puts 'Using environment: ' + env.to_s unless silent
    Network::set_domain Pref.config[:environments][env]['domain']
    Pref.config[:token] = Pref.config[:environments][env]['token']
    Pref.config[:safe_mode] = Pref.config[:environments][env]['safe_mode']
    Pref.config[:scheme] = Pref.config[:environments][env]['scheme']
    Pref.config[:suppress_progress] = Pref.config[:environments][env]['progress_suppression'] ?
        Pref.config[:environments][env]['progress_suppression'] : Pref.config[:default_progress_suppression]
    Pref.config[:suppress_progress].map! { |a| a.to_sym }
    CanvasToolkit::update_prompt
    return true
  end
  false
end

def use_default_environment(*args)
  set_environment Pref.config[:default_environment], *args
end

def use_default_test_environment(*args)
  set_environment Pref.config[:default_test_environment], *args
end

def masquerade(as, &block)
  as = HelperBase::get_id(as)
  if block_given?
    begin
      Pref.config[:tmp_masquerade] = Pref.config[:masquerade]
      Pref.config[:masquerade] = as
      yield
    ensure
      Pref.config[:masquerade] = Pref.config[:tmp_masquerade]
    end
  else
    Pref.config[:masquerade] = as
  end
  CanvasToolkit::update_prompt
  Pref.config[:masquerade]
end

def suppress_safe_mode(&block)
  if block_given?
    tmp = Pref.config[:safe_mode]
    Pref.config[:safe_mode] = false
    begin
      yield
    ensure
      Pref.config[:safe_mode] = tmp
    end
  end
end

def stop_masquerading
  masquerade(nil)
end

def masquerading?
  !!Pref.config[:masquerade]
end

def masquerading_as
  Pref.config[:masquerade]
end

def ui_enabled?
  !!defined?(UI)
end

# Shortcut for ScriptLoader.run
def run(script)
  ScriptLoader.run script
end

alias :masq :masquerade
alias :become :masquerade
alias :masq? :masquerading?
alias :stop_masq :stop_masquerading
alias :masq_as :masquerading_as
alias :set_env :set_environment
alias :use_default_env :use_default_environment
alias :use_default_test_env :use_default_test_environment
