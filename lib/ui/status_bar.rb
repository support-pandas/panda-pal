java_import javax.swing.JPanel
java_import javax.swing.JLabel
java_import javax.swing.BoxLayout
java_import javax.swing.BorderFactory

class StatusBar < JPanel

  attr_accessor :env_label
  attr_accessor :domain_label
  attr_accessor :masquerade_label

  def initialize
    super
    set_border BorderFactory.createEtchedBorder(javax.swing.border.BevelBorder::RAISED)
    set_layout BoxLayout.new self, BoxLayout::X_AXIS
    self.env_label = JLabel.new ''
    env_label.set_foreground java.awt.Color::blue

    filler1 = javax.swing.Box::Filler.new(java.awt.Dimension.new(10, 0), java.awt.Dimension.new(10, 0), java.awt.Dimension.new(10, 32767))
    self.domain_label = JLabel.new ''

    filler2 = javax.swing.Box::Filler.new(java.awt.Dimension.new(10, 0), java.awt.Dimension.new(10, 0), java.awt.Dimension.new(10, 32767))
    self.masquerade_label = JLabel.new ''
    self.masquerade_label.set_foreground java.awt.Color::red
    self.masquerade_label.set_visible false

    self.add env_label
    self.add filler1
    self.add domain_label
    self.add filler2
    self.add masquerade_label
  end

  def refresh
    self.env_label.set_text "Environment: #{Pref.config[:environment].to_s}"
    self.domain_label.set_text "Domain: #{domain}"

    self.masquerade_label.set_visible masq?
    self.masquerade_label.set_text "Masquerading as: #{masq_as}"
  end

end