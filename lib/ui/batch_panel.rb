java_import javax.swing.JPanel
java_import javax.swing.SpringLayout
java_import javax.swing.JTree
java_import javax.swing.JScrollPane
java_import javax.swing.tree.TreePath
java_import javax.swing.tree.DefaultTreeModel
java_import javax.swing.JOptionPane
java_import javax.swing.JButton
java_import javax.swing.JCheckBox
require_relative 'shared/search_panel.rb'

class BatchPanel < JPanel
  include SearchPanel

  attr_accessor :sl
  attr_accessor :search_panel
  attr_accessor :tree
  attr_accessor :tree_model
  attr_accessor :tree_selection_model
  attr_accessor :property_sheet
  attr_accessor :prop_registry
  attr_accessor :update_btn
  attr_accessor :update_check_box

  def initialize
    super
    self.sl = SpringLayout.new
    set_layout sl
    do_layout
  end

  def enable_components(enable)
    tree.set_enabled enable
    search_field.set_enabled enable
    search_btn.set_enabled enable
    search_btn_group.get_elements.each { |b| b.set_enabled enable }
    property_sheet.set_enabled enable
    if enable
      refresh
    else
      update_check_box.set_enabled false
      update_btn.set_enabled false
    end
  end

  def refresh
    if Pref.config[:safe_mode]
      update_check_box.set_enabled true
      update_btn.set_enabled update_check_box.is_selected && changed_nodes.length > 0
    else
      update_check_box.set_enabled false
      update_btn.set_enabled changed_nodes.length > 0
    end
    if changed_nodes.length > 0
      update_btn.set_text 'Update ' + changed_nodes.length.to_s + ' Items'
    else
      update_btn.set_text 'Update Items'
    end
  end

  def selected_nodes
    tree_selection_model.get_selection_paths.to_a.map do |path|
      path.get_last_path_component
    end
  end

  def set_node_changed(node, node_itself_changed = true, do_refresh = true)
    node.changed = node_itself_changed
    tree_model.node_changed node
    tree_selection_model.refresh_property_sheet
    refresh if do_refresh
  end

  private
  def do_layout
    self.search_panel = layout_search_panel(:do_search)
    self.tree = JTree.new
    self.tree_selection_model = BatchTreeSelectionModel.new
    tree.set_selection_model tree_selection_model
    reset_tree_model

    tree.add_tree_expansion_listener do |evt|
      UI.job_thread = Thread.new do
        begin
          UI.frame.enable_components false
          evt.get_path.get_last_path_component.handle_expansion(evt)
        rescue Exception => e
          UI.handle_exception e
        ensure
          UI.frame.enable_components true
        end
      end
    end

    tree.add_mouse_listener do |evt|
      if evt.get_id == java.awt.event.MouseEvent::MOUSE_CLICKED
        node = tree.get_path_for_location evt.get_x, evt.get_y
        if node
          tree_selection_model.addSelectionPath(node)
        end

        if !selected_nodes.empty? && selected_nodes.all? { |s| s.helper? } && evt.get_button == java.awt.event.MouseEvent::BUTTON3
          case selected_nodes.first.get_user_object
            when Course
              p = CoursePopupMenu.new
            when Account
              p = AccountPopupMenu.new
            else
              p = PalPopupMenu.new
          end
          p.show evt.get_component, evt.get_x, evt.get_y
        end
      end
    end

    tree_scroll = JScrollPane.new tree

    self.property_sheet = com.l2fprod.common.propertysheet.PropertySheetPanel.new
    self.prop_registry = com.l2fprod.common.propertysheet.PropertyEditorRegistry.new
    prop_registry.register_defaults
    prop_registry.register_editor java.lang.String, com.l2fprod.common.beans.editor.StringPropertyEditor.new
    property_sheet.set_editor_factory prop_registry
    property_sheet.add_property_sheet_change_listener do |evt|
      tree_selection_model.get_selection_paths.to_a.each do |path|
        node = path.get_last_path_component
        obj = node.get_user_object
        obj.method("#{evt.get_source.get_name}=").call evt.get_new_value
        node.changed = true
        tree_model.node_changed node
      end
      refresh
    end
    add property_sheet

    self.update_check_box = JCheckBox.new('Confirm Update')
    update_check_box.set_tool_tip_text 'This operation is potentially dangerous.  By selecting this checkbox, it is agreed that any actions undertaken are entirely voluntary, and cannot be used to hold the authors liable.'
    update_check_box.add_action_listener do
      if changed_nodes.length > 0
        update_btn.set_enabled update_check_box.is_selected
      end
    end
    add update_check_box

    self.update_btn = JButton.new('Update Items')
    update_btn.set_enabled false
    update_btn.add_action_listener do
      UI.job_thread = Thread.new do
        begin
          UI.frame.enable_components false
          Pref.config[:safe_mode_suppressed] = true
          do_update
        rescue Exception => e
          UI.handle_exception e
        ensure
          UI.frame.enable_components true
          Pref.config[:safe_mode_suppressed] = false
        end
      end
    end
    add update_btn

    add search_panel
    add tree_scroll

    sl.put_constraint(SpringLayout::NORTH, search_panel, 0, SpringLayout::NORTH, self)
    sl.put_constraint(SpringLayout::SOUTH, search_panel, 90, SpringLayout::NORTH, self)
    sl.put_constraint(SpringLayout::WEST, search_panel, 0, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::EAST, search_panel, 0, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::NORTH, tree_scroll, 5, SpringLayout::SOUTH, search_panel)
    sl.put_constraint(SpringLayout::SOUTH, tree_scroll, -5, SpringLayout::SOUTH, self)
    sl.put_constraint(SpringLayout::WEST, tree_scroll, 5, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::EAST, tree_scroll, -5, SpringLayout::HORIZONTAL_CENTER, self)

    sl.put_constraint(SpringLayout::NORTH, property_sheet, 5, SpringLayout::SOUTH, search_panel)
    sl.put_constraint(SpringLayout::SOUTH, property_sheet, -5, SpringLayout::NORTH, update_btn)
    sl.put_constraint(SpringLayout::WEST, property_sheet, 5, SpringLayout::HORIZONTAL_CENTER, self)
    sl.put_constraint(SpringLayout::EAST, property_sheet, -5, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::SOUTH, update_btn, -5, SpringLayout::SOUTH, self)
    sl.put_constraint(SpringLayout::EAST, update_btn, -5, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::BASELINE, update_check_box, 0, SpringLayout::BASELINE, update_btn)
    sl.put_constraint(SpringLayout::EAST, update_check_box, -5, SpringLayout::WEST, update_btn)
  end

  def do_search(type, term)
    reset_tree_model
    UI.job_thread = Thread.new do
      begin
        UI.frame.enable_components false
        if type == 'Accounts'
          helper_obj = HelperTreeNode.new(Account.find(term))
          helper_obj.add_children
        end
        if type == 'Courses'
          helper_obj = HelperTreeNode.new('Courses')
          helper_obj.searched = true
          if term.match(/\d+/)
            course = [Course.find(term)]
          else
            course = Account.root.courses('search_term=' << term)
          end
          course.each do |c|
            node = HelperTreeNode.new(c)
            helper_obj.add node
            node.add_children
          end
          tree.expand_path TreePath.new(helper_obj.get_path)
        end
        tree_model.set_root helper_obj
        tree_model.reload
        refresh
      rescue Exception => e
        UI.handle_exception e
      ensure
        UI.frame.enable_components true
      end
    end
  end

  def reset_tree_model
    self.tree_model = DefaultTreeModel.new nil, true
    tree.set_model tree_model
  end

  def get_nodes(start_node = tree_model.get_root)
    return [] if start_node.nil?
    nodes = []
    unless start_node.is_leaf
      start_node.children.to_a.each do |child|
        nodes += get_nodes(child)
      end
    end
    nodes << start_node
    nodes
  end

  def changed_nodes
    get_nodes.select { |n| n.changed? }
  end

  def do_update
    updated_objects = changed_nodes
    updated_objects = Batch.new updated_objects
    with_errors = []
    updated_objects.each do |n|
      o = n.get_user_object
      begin
        status = o.get(:status)
        o.update
        case status
          when :deleted
            tree_model.remove_node_from_parent n
            tree_selection_model.refresh_property_sheet
          else
            set_node_changed n, false, false
        end
      rescue Exception => e
        with_errors << o
        puts o.class.name + ' ' + o.get(:id)
        puts e.message
        puts e.backtrace
      end
    end
  ensure
    # Cleanup
    update_check_box.set_selected false
    refresh

    # TODO: Display dialog with objects that raised an error while updating
  end

end