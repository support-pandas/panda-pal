require File.expand_path('pal_popup_menu.rb', File.dirname(__FILE__))

class CoursePopupMenu < PalPopupMenu
  def initialize
    super

    add_new_item
    assignment = JMenuItem.new('Assignment')
    assignment.add_action_listener do
      handle_new_object_creation(method: :create_assignment, node_name: 'Assignments')
    end
    new_item.add assignment

    add_publishable_items
    add_delete_item
  end
end