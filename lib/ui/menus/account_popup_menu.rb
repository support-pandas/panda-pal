require File.expand_path('pal_popup_menu.rb', File.dirname(__FILE__))

class AccountPopupMenu < PalPopupMenu
  def initialize
    super
    add_new_item
    course = JMenuItem.new('Course')
    course.add_action_listener do
      handle_new_object_creation(method: :create_course, node_name: 'Courses')
    end
    new_item.add course
  end
end