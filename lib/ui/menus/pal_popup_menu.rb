java_import javax.swing.JPopupMenu
java_import javax.swing.JMenuItem
java_import javax.swing.JMenu
java_import javax.swing.JOptionPane

class PalPopupMenu < JPopupMenu

  attr_accessor :new_item

  def initialize
    super
    reset = JMenuItem.new('Reset')
    reset.add_action_listener do
      UI.frame.batch_tab.selected_nodes.each do |n|
        obj = n.get_user_object
        obj.reset
        UI.frame.batch_tab.set_node_changed n, false
      end
    end
    add reset
  end

  def add_publishable_items(with_separator = true)
    add_separator if with_separator

    publish = JMenuItem.new 'Publish'
    publish.add_action_listener do
      UI.frame.batch_tab.selected_nodes.each do |n|
        obj = n.get_user_object
        obj.publish
        UI.frame.batch_tab.set_node_changed n
      end
    end
    add publish

    unpublish = JMenuItem.new 'Unpublish'
    unpublish.add_action_listener do
      UI.frame.batch_tab.selected_nodes.each do |n|
        obj = n.get_user_object
        obj.unpublish
        UI.frame.batch_tab.set_node_changed n
      end
    end
    add unpublish
  end

  def add_delete_item(with_separator = true)
    add_separator if with_separator

    delete = JMenuItem.new 'Delete'
    delete.add_action_listener do
      UI.frame.batch_tab.selected_nodes.each do |n|
        obj = n.get_user_object
        if obj.new_obj?
          UI.frame.batch_tab.tree_model.remove_node_from_parent n
          UI.frame.batch_tab.refresh
        else
          obj.destroy
          UI.frame.batch_tab.set_node_changed n
        end
      end
    end
    add delete
  end

  def add_new_item(with_separator = true)
    add_separator if with_separator
    self.new_item = JMenu.new 'New'
    add new_item

    if UI::frame.batch_tab.selected_nodes.any? { |n| n.helper? && n.get_user_object.new_obj? }
      new_item.set_enabled(false)
      new_item.set_tool_tip_text 'New items cannot be added to this one, as it is itself new'
    end
  end

  def ask_for_amount
    amount = JOptionPane.show_input_dialog(UI::frame, 'How many would you like to create?', 1)
    return nil if amount.nil?
    amount.to_i
  end

  # Params:
  # - opts
  # -- :method: Method name, such as :create_assignment
  # -- :node_name: The name of the child name to create the new object node within, such as Courses
  # -- :amount: :ask to query the user for an amount, or a fixnum to skip the request
  # -- :default_name: The name given to newly created objects
  def handle_new_object_creation(opts = {})
    raise ConfigurationError.new('A method name must be included') unless opts[:method]
    raise ConfigurationError.new('A name for the parent node must be included') unless opts[:node_name]
    opts[:amount] ||= :ask
    opts[:default_name] ||= opts[:node_name].to_s.singularize

    amount = opts[:amount] == :ask ? ask_for_amount : opts[:amount].to_i
    return if amount.nil?
    amount.times do |i|
      UI.frame.batch_tab.selected_nodes.each do |parent|
        node = parent.get_child_by_name(opts[:node_name].to_s)
        sub = HelperTreeNode.new(parent.get_user_object.method(opts[:method]).call(name: "#{opts[:default_name]} #{i + 1}"), changed: true)
        sub.add_children
        UI.frame.batch_tab.tree_model.insert_node_into(sub, node, node.get_child_count)
      end
    end
    UI.frame.batch_tab.refresh
  end
end