java_import javax.swing.JFrame
java_import javax.swing.WindowConstants
java_import javax.swing.JPanel
java_import javax.swing.JTabbedPane
java_import javax.swing.SpringLayout
java_import javax.swing.JMenuBar
java_import javax.swing.JMenu
java_import javax.swing.JMenuItem
java_import javax.swing.JRadioButtonMenuItem
java_import java.awt.event.KeyEvent
java_import javax.swing.KeyStroke
java_import javax.swing.Timer

# class name avoids ambiguity with java.awt.Frame
class PandaPalFrame < JFrame
  include java.awt.event.ActionListener

  attr_accessor :tab_pane
  attr_accessor :batch_tab
  attr_accessor :individual_tab
  attr_accessor :scripts_tab
  attr_accessor :settings_tab
  attr_accessor :status_bar
  attr_accessor :menu
  attr_accessor :env_menu
  attr_accessor :config_menu
  attr_accessor :stop_masquerading_item
  attr_accessor :progress_panel

  def initialize(*attr)
    super *attr
    self.default_close_operation = WindowConstants.EXIT_ON_CLOSE

    resize_timer = Timer.new 500, self
    resize_timer.set_repeats false
    self.add_component_listener do |e|
      if e.get_id == java.awt.event.ComponentEvent::COMPONENT_RESIZED
        if resize_timer.is_running
          resize_timer.restart
        else
          resize_timer.start
        end
      end
    end

    sl = SpringLayout.new
    self.set_layout sl
    self.tab_pane = JTabbedPane.new

    self.batch_tab = BatchPanel.new
    self.individual_tab = JPanel.new
    self.scripts_tab = JPanel.new
    self.settings_tab = SettingsPanel.new
    self.status_bar = StatusBar.new

    self.add tab_pane
    self.add status_bar

    self.progress_panel = ProgressPanel.new
    self.add progress_panel

    sl.put_constraint(SpringLayout::WEST, tab_pane, 0, SpringLayout::WEST, get_content_pane)
    sl.put_constraint(SpringLayout::EAST, tab_pane, 0, SpringLayout::EAST, get_content_pane)
    sl.put_constraint(SpringLayout::NORTH, tab_pane, 0, SpringLayout::NORTH, get_content_pane)
    sl.put_constraint(SpringLayout::SOUTH, tab_pane, 0, SpringLayout::NORTH, progress_panel)

    sl.put_constraint(SpringLayout::WEST, progress_panel, 0, SpringLayout::WEST, get_content_pane)
    sl.put_constraint(SpringLayout::EAST, progress_panel, 0, SpringLayout::EAST, get_content_pane)
    sl.put_constraint(SpringLayout::NORTH, progress_panel, -25, SpringLayout::NORTH, status_bar)
    sl.put_constraint(SpringLayout::SOUTH, progress_panel, 0, SpringLayout::NORTH, status_bar)

    sl.put_constraint(SpringLayout::WEST, status_bar, 0, SpringLayout::WEST, get_content_pane)
    sl.put_constraint(SpringLayout::EAST, status_bar, 0, SpringLayout::EAST, get_content_pane)
    sl.put_constraint(SpringLayout::NORTH, status_bar, -20, SpringLayout::SOUTH, get_content_pane)
    sl.put_constraint(SpringLayout::SOUTH, status_bar, 0, SpringLayout::SOUTH, get_content_pane)

    self.menu = JMenuBar.new
    self.env_menu = JMenu.new('Environments')

    self.config_menu = JMenu.new('Configuration')
    domain_item = JMenuItem.new('Set Domain')
    domain_item.add_action_listener do
      domain javax.swing.JOptionPane.show_input_dialog(self, 'Enter a new domain address', domain)
      status_bar.refresh
    end
    domain_item.set_accelerator KeyStroke.get_key_stroke(KeyEvent::VK_D, UI::control_mask)
    config_menu.add domain_item

    masquerade_item = JMenuItem.new('Masquerade As')
    masquerade_item.add_action_listener do
      m = javax.swing.JOptionPane.show_input_dialog(self, 'Enter a user ID to masquerade as', masq_as)
      if m && !m.empty?
        masq m
      else
        stop_masq
      end
      refresh_all_views
    end
    masquerade_item.set_accelerator KeyStroke.get_key_stroke(KeyEvent::VK_M, UI::control_mask)
    config_menu.add masquerade_item

    self.stop_masquerading_item = JMenuItem.new('Stop Masquerading')
    stop_masquerading_item.add_action_listener do
      stop_masq
      refresh_all_views
    end
    stop_masquerading_item.set_accelerator KeyStroke.get_key_stroke(KeyEvent::VK_M, UI::control_mask | KeyEvent::SHIFT_DOWN_MASK)
    config_menu.add stop_masquerading_item

    refresh_all_views
    self.menu.add config_menu
    self.menu.add env_menu
    self.set_jmenu_bar menu

    #tab_pane.add_tab 'Individual', individual_tab
    tab_pane.add_tab 'Batch', batch_tab
    #tab_pane.add_tab 'Scripts', scripts_tab
    tab_pane.add_tab 'Settings', settings_tab
    add_progress_hooks
  end

  def refresh_all_views
    add_env_menu_items
    refresh_menu
    status_bar.refresh
    batch_tab.refresh
  end

  def refresh_menu
    self.env_menu.get_menu_components.each do |i|
      i.set_selected false
    end
    self.env_menu.get_menu_components.find { |i| i.get_text == Pref.config[:environment].to_s }.set_selected true
    self.stop_masquerading_item.set_enabled masq?
  end

  def add_env_menu_items
    env_menu.remove_all
    Pref.config[:environments].keys.each do |k|
      item = JRadioButtonMenuItem.new k.to_s
      item.add_action_listener do |event|
        set_env event.get_source.get_text.to_sym, true
        refresh_all_views
      end
      env_menu.add item
    end
  end

  def add_progress_hooks
    fin = proc do
      progress_panel.finish
    end
    Progress.finish_hook = fin

    Progress.add_refresh_hook do |bar|
      opts = { status: bar.title, elapsed: bar.time.elapsed_with_label, estimated: bar.time.estimated_with_label }
      if Progress.known?
        opts.merge! type: :known, percentage: bar.progressable.percentage_completed
      else
        opts.merge! type: :unknown, count: Progress.current_count
      end
      progress_panel.update opts
    end
  end

  def enable_components(enable)
    env_menu.set_enabled enable
    config_menu.set_enabled enable
    tab_pane.set_enabled enable

    batch_tab.enable_components enable
  end

  # Non-conventional naming, as this is what java expects (camel-casing)
  def actionPerformed(ae)
    if ae.get_source.is_a?(javax.swing.Timer)
      Pref.ui[:frame_dimensions] = [self.get_width, self.get_height]
      Pref::save
    end
  end
end