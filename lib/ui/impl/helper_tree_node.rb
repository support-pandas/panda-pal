java_import javax.swing.tree.DefaultMutableTreeNode

class HelperTreeNode < DefaultMutableTreeNode

  attr_accessor :searched
  attr_accessor :changed

  def initialize(obj, opts = {})
    opts[:allows_children] ||= true
    opts[:changed] ||= false
    super obj, opts[:allows_children]
    self.changed = opts[:changed]
  end

  def searched?
    !!searched
  end

  def changed?
    !!changed
  end

  def helper?
    get_user_object.is_a?(HelperBase)
  end

  # params:
  # - evt: TreeExpansionEvent
  #   See: http://docs.oracle.com/javase/7/docs/api/javax/swing/event/TreeExpansionEvent.html
  def handle_expansion(evt)
    return if searched?

    obj = get_helper_parent.courses if get_user_object == 'Courses'
    obj = get_helper_parent.sub_accounts if get_user_object == 'Accounts'
    obj = get_helper_parent.assignments if get_user_object == 'Assignments'

    do_expansion(obj) if obj
    self.searched = true
    UI.frame.batch_tab.tree_model.reload self
  end

  def get_helper_parent
    path = get_user_object_path.to_a.compact
    path.pop # Remove the last item, as it refers to this node
    path.reverse.each do |p|
      return p if p.is_a?(HelperBase)
    end
  end

  # Java name
  def toString
    str = ''
    str += '*' if changed?
    if get_user_object.is_a?(HelperBase)
      str << '(deleted) ' if get_user_object.get(:status) == :deleted
      str << '(new) ' if get_user_object.new_obj?
      str << get_user_object.get(:name)
    else
      str << get_user_object
    end
    str
  end

  def add_children
    return unless helper?
    if get_user_object.class.const_defined?(:CHILDREN)
      get_user_object.class.const_get(:CHILDREN).each { |child| self.add HelperTreeNode.new(Misc::pluralize(child.name)) }
    end
  end

  def get_child_by_name(name)
    children.each do |child|
      return child if child.toString == name
    end
  end

  private
  def do_expansion(obj)
    obj.each do |o|
      node = HelperTreeNode.new(o)
      self.add node
      if o.class.const_defined?(:CHILDREN)
        o.class.const_get(:CHILDREN).each do |child|
          node.add HelperTreeNode.new(Misc::pluralize(child.name))
        end
      end
    end
  end
end