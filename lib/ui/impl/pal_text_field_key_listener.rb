java_import java.awt.event.KeyListener
java_import java.awt.event.KeyEvent
java_import java.lang.System
java_import javax.swing.UIManager
java_import java.awt.Robot

class PalTextFieldKeyListener
  include KeyListener

  def key_pressed(event)
    # Mac is lame when it comes to pasting into text fields with L&F other than the Mac default
    #
    # This is meant to emulate normal control/meta key functionality with regard to the clipboard
    if System.get_properties['os.name'] == 'Mac OS X' && UIManager.get_look_and_feel.get_name != 'Mac OS X'
      if event.is_meta_down
        case event.get_key_code
          when KeyEvent::VK_V, KeyEvent::VK_C, KeyEvent::VK_A, KeyEvent::VK_Z, KeyEvent::VK_Y, KeyEvent::VK_X
            do_key_event event.get_key_code
        end
      end
    end
  end

  def key_typed(event)
  end

  def key_released(event)
  end

  private
  def do_key_event(key)
    r = Robot.new
    r.key_press KeyEvent::VK_CONTROL
    r.key_press key
    r.key_release key
    r.key_release KeyEvent::VK_CONTROL
  end
end