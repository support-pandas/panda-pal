java_import javax.swing.tree.DefaultTreeSelectionModel

class BatchTreeSelectionModel < DefaultTreeSelectionModel

  def addSelectionPath(path)
    # Standard null check
    return unless get_selection_path
    # Require selected nodes to be of the same type (don't allow accounts and courses to be selected at the same time)
    return unless get_selection_path.get_last_path_component.get_user_object.class == path.get_last_path_component.get_user_object.class

    unless path.get_last_path_component.get_user_object.is_a?(HelperBase)
      # Allow multiple header nodes to be selected, but only if they're headers for the same type
      # For example, multiple course headers can be selected under an account, but not course + user headers
      return unless get_selection_path.get_last_path_component.get_user_object == path.get_last_path_component.get_user_object
    end
    super
  end

  def setSelectionPaths(paths)
    first = nil
    paths.to_a.each do |p|
      first ||= p.get_last_path_component.get_user_object
      return unless first.class == p.get_last_path_component.get_user_object.class
    end
    super
  end

  def setSelectionPath(path)
    super
    refresh_property_sheet
  end

  def refresh_property_sheet
    UI.frame.batch_tab.property_sheet.set_properties [].to_java com.l2fprod.common.propertysheet.Property
    if get_selection_count == 1
      if get_selection_path.get_last_path_component.get_user_object.is_a?(HelperBase)
        obj = get_selection_path.get_last_path_component.get_user_object
        data = obj.new_obj? ? Pref.attributes[obj.class.name].map { |k, v| { k => obj.get(k) } }.reduce(:merge) : obj.data
        data.each do |k, v|
          next if v.is_a?(Array) || v.is_a?(Hash)

          prop = com.l2fprod.common.propertysheet.DefaultProperty.new
          prop.set_name k
          prop.set_display_name k
          prop.set_value v.to_s
          if Pref.attributes[obj.class.name] && Pref.attributes[obj.class.name][k.to_s] && Pref.attributes[obj.class.name][k.to_s]['access'] != 'read_only'
            prop.set_editable true
            UI.frame.batch_tab.prop_registry.register_editor prop, com.l2fprod.common.beans.editor.StringPropertyEditor.new
          end

          UI.frame.batch_tab.property_sheet.add_property prop
        end
      end
    end
  end
end