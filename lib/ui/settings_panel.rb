java_import javax.swing.JPanel
java_import javax.swing.JLabel
java_import javax.swing.SpringLayout
java_import javax.swing.JComboBox
java_import javax.swing.UIManager
java_import javax.swing.SwingUtilities
java_import javax.swing.JTextField
java_import javax.swing.JButton
java_import javax.swing.JCheckBox
java_import javax.swing.JOptionPane

class SettingsPanel < JPanel

  attr_accessor :sl
  attr_accessor :token_text_field
  attr_accessor :env_box
  attr_accessor :env_button
  attr_accessor :domain_field
  attr_accessor :safe_mode_check_box
  attr_accessor :scheme_check_box
  attr_accessor :new_env_button
  attr_accessor :remove_env_button

  def initialize
    super
    self.sl = SpringLayout.new
    set_layout sl
    do_layout
  end

  private
  def do_layout
    theme_label = JLabel.new('UI Theme')
    lf = UIManager.get_installed_look_and_feels.collect { |u| u.get_name }.to_java :string
    theme_box = JComboBox.new lf

    theme_box.set_selected_item UIManager.get_look_and_feel.get_name

    theme_box.add_action_listener do
      new_lf = get_lf_for_name(theme_box.get_selected_item.to_s)
      if new_lf
        Pref.ui[:default_lf] = new_lf
        Pref::save
        UIManager.set_look_and_feel new_lf
        SwingUtilities.update_component_tree_ui UI::frame
      end
    end

    env_label = JLabel.new('Environments')
    self.env_box = JComboBox.new
    populate_env_selector
    env_box.set_selected_item Pref.config[:environment].to_s
    env_box.add_action_listener do
      reload_fields env_box.get_selected_item.to_sym if env_box.get_item_count > 0
    end
    self.env_button = JButton.new 'Set as default'
    env_button.add_action_listener do
      Pref.config[:default_environment] = env_box.get_selected_item.to_sym
      Pref::save
      env_box.request_focus_in_window
      reload_fields(env_box.get_selected_item.to_sym)
    end

    token_label = JLabel.new('API Token')
    self.token_text_field = JTextField.new
    token_text_field.add_focus_listener do |f|
      if f.get_id == java.awt.event.FocusEvent::FOCUS_LOST
        Pref.config[:environments][env_box.get_selected_item.to_sym]['token'] = token_text_field.get_text
        Pref::save
      end
      token_text_field.set_text get_token_for_field(f.get_id == java.awt.event.FocusEvent::FOCUS_GAINED)
      token_text_field.set_caret_position 0 if f.get_id == java.awt.event.FocusEvent::FOCUS_LOST
    end
    token_text_field.add_key_listener PalTextFieldKeyListener.new

    domain_label = JLabel.new('Domain')
    self.domain_field = JTextField.new
    domain_field.add_focus_listener do |f|
      if f.get_id == java.awt.event.FocusEvent::FOCUS_LOST
        dom = Network::set_domain(domain_field.get_text, true)
        domain_field.set_text dom
        Pref.config[:environments][env_box.get_selected_item.to_sym]['domain'] = dom
        Pref::save
      end
    end
    domain_field.add_key_listener PalTextFieldKeyListener.new

    safe_mode_label = JLabel.new 'Safe Mode'
    self.safe_mode_check_box = JCheckBox.new ''
    safe_mode_check_box.add_action_listener do
      Pref.config[:environments][env_box.get_selected_item.to_sym]['safe_mode'] = safe_mode_check_box.is_selected
      Pref::save
      reload_fields env_box.get_selected_item.to_sym
    end

    scheme_label = JLabel.new 'Use HTTPS'
    self.scheme_check_box = JCheckBox.new ''
    scheme_check_box.add_action_listener do
      Pref.config[:environments][env_box.get_selected_item.to_sym]['scheme'] = (scheme_check_box.is_selected ? 'https' : 'http')
      Pref::save
    end

    self.new_env_button = JButton.new '+'
    new_env_button.add_action_listener do
      name = JOptionPane.show_input_dialog(UI::frame, 'Input a name for the new environment')
      if name && !name.empty?
        if Pref.config[:environments].keys.any? { |k| k.to_s == name }
          JOptionPane.show_message_dialog(UI::frame, 'An environment with this name already exists')
        else
          Pref.config[:environments][name.to_sym] = { 'domain' => '', 'token' => '', 'safe_mode' => true, 'scheme' => 'https' }
          Pref::save
          populate_env_selector
          env_box.set_selected_item name
          reload_fields name.to_sym
          UI::frame.refresh_all_views
        end
      end
    end

    self.remove_env_button = JButton.new '-'
    remove_env_button.add_action_listener do
      result = JOptionPane.show_confirm_dialog(UI::frame, 'Are you sure you want to delete this environment?', 'Confirm', JOptionPane::YES_NO_OPTION)
      if result == JOptionPane::YES_OPTION
        env = env_box.get_selected_item
        Pref.config[:environments].delete env.to_sym

        # Change the environment, if the deleted one was active
        set_env(Pref.config[:environments].first[0], true) if env.to_sym == Pref.config[:environment]

        # Change default env setting if deleted was set as default
        Pref.config[:default_environment] = Pref.config[:environment] if Pref.config[:default_environment] == env.to_sym

        Pref::save
        populate_env_selector
        env_box.set_selected_index 0
        reload_fields(env_box.get_selected_item.to_sym)
        UI::frame.refresh_all_views
      end
    end

    reload_fields env_box.get_selected_item.to_sym

    self.add theme_label
    self.add theme_box

    self.add env_label
    self.add env_button
    self.add env_box

    self.add token_label
    self.add token_text_field

    self.add domain_label
    self.add domain_field

    self.add safe_mode_label
    self.add safe_mode_check_box

    self.add scheme_label
    self.add scheme_check_box

    self.add new_env_button
    self.add remove_env_button

    sl.put_constraint(SpringLayout::NORTH, theme_label, 15, SpringLayout::NORTH, self)
    sl.put_constraint(SpringLayout::EAST, theme_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, theme_box, 5, SpringLayout::EAST, theme_label)
    sl.put_constraint(SpringLayout::BASELINE, theme_box, 0, SpringLayout::BASELINE, theme_label)

    sl.put_constraint(SpringLayout::NORTH, env_label, 15, SpringLayout::SOUTH, theme_label)
    sl.put_constraint(SpringLayout::EAST, env_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, env_box, 5, SpringLayout::EAST, env_label)
    sl.put_constraint(SpringLayout::BASELINE, env_box, 0, SpringLayout::BASELINE, env_label)
    sl.put_constraint(SpringLayout::EAST, env_button, -50, SpringLayout::EAST, self)
    sl.put_constraint(SpringLayout::BASELINE, env_button, 0, SpringLayout::BASELINE, env_label)

    sl.put_constraint(SpringLayout::NORTH, token_label, 15, SpringLayout::SOUTH, env_label)
    sl.put_constraint(SpringLayout::EAST, token_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, token_text_field, 5, SpringLayout::EAST, token_label)
    sl.put_constraint(SpringLayout::BASELINE, token_text_field, 0, SpringLayout::BASELINE, token_label)
    sl.put_constraint(SpringLayout::EAST, token_text_field, -50, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::NORTH, domain_label, 15, SpringLayout::SOUTH, token_label)
    sl.put_constraint(SpringLayout::EAST, domain_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, domain_field, 5, SpringLayout::EAST, domain_label)
    sl.put_constraint(SpringLayout::BASELINE, domain_field, 0, SpringLayout::BASELINE, domain_label)
    sl.put_constraint(SpringLayout::EAST, domain_field, -50, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::NORTH, safe_mode_label, 15, SpringLayout::SOUTH, domain_label)
    sl.put_constraint(SpringLayout::EAST, safe_mode_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, safe_mode_check_box, 5, SpringLayout::EAST, safe_mode_label)
    sl.put_constraint(SpringLayout::SOUTH, safe_mode_check_box, 1, SpringLayout::SOUTH, safe_mode_label)

    sl.put_constraint(SpringLayout::NORTH, scheme_label, 15, SpringLayout::SOUTH, safe_mode_label)
    sl.put_constraint(SpringLayout::EAST, scheme_label, 150, SpringLayout::WEST, self)
    sl.put_constraint(SpringLayout::WEST, scheme_check_box, 5, SpringLayout::EAST, scheme_label)
    sl.put_constraint(SpringLayout::SOUTH, scheme_check_box, 1, SpringLayout::SOUTH, scheme_label)

    sl.put_constraint(SpringLayout::NORTH, new_env_button, 15, SpringLayout::SOUTH, scheme_label)
    sl.put_constraint(SpringLayout::EAST, new_env_button, -50, SpringLayout::EAST, self)

    sl.put_constraint(SpringLayout::NORTH, remove_env_button, 15, SpringLayout::SOUTH, scheme_label)
    sl.put_constraint(SpringLayout::EAST, remove_env_button, -2, SpringLayout::WEST, new_env_button)
  end

  def get_lf_for_name(lf_name)
    lf = UIManager.get_installed_look_and_feels.find { |u| u.get_name == lf_name }
    lf.get_class_name if lf
  end

  def get_token_for_field(show = false, token = nil)
    token ||= Pref.config[:environments][env_box.get_selected_item.to_sym]['token']
    token = token.to_s
    if show
      token
    else
      if token.match /~/
        match = token.match /(\d+~.{5})(.*)/
      else
        match = token.match /(.{5})(.*)/
      end
      match[1] << match[2].gsub(/./, '*') unless match.nil?
    end
  end

  def reload_fields(env)
    token_text_field.set_text get_token_for_field(false, Pref.config[:environments][env]['token'])
    env_button.set_enabled Pref.config[:default_environment] != env
    domain_field.set_text Pref.config[:environments][env]['domain']
    safe_mode_check_box.set_selected Pref.config[:environments][env]['safe_mode']
    safe_mode_check_box.set_text (Pref.config[:environments][env]['safe_mode'] ? 'Enabled' : 'Disabled')
    scheme_check_box.set_selected Pref.config[:environments][env]['scheme'] == 'https'
    remove_env_button.set_enabled Pref.config[:environments].length > 1
    remove_env_button.set_tool_tip_text(Pref.config[:environments].length > 1 ? nil : 'There must always be at least one environment.')
  end

  def populate_env_selector
    env_box.remove_all_items
    Pref.config[:environments].keys.each { |k| env_box.add_item k.to_s }
  end

end