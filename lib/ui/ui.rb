module UI

  def self.initialize
    Pref.load_ui_prefs

    require 'java'
    # Require entirety of /lib/ui folder
    Dir.glob(File.dirname(__FILE__) + '/**/*.rb', &method(:require))
    Dir.glob(File.dirname(__FILE__) + '/../../deps/*.jar', &method(:require))

    java_import javax.swing.UIManager

    begin
      UIManager.set_look_and_feel Pref.ui[:default_lf]
    rescue
      UIManager.set_look_and_feel UIMananger.get_system_look_and_feel_class_name
    end

    # Invoke the thread which loads the UI in the event dispatch thread, to avoid synchronization issues
    javax.swing.SwingUtilities.invoke_later UI
  end

  # This flips to true when the frame and all components are fully loaded
  def self.ui_loaded?
    !!UI.ui_loaded
  end

  class << self
    include java.lang.Runnable
    attr_accessor :frame
    attr_accessor :ui_loaded
    attr_accessor :job_thread

    def run
      UI.frame = PandaPalFrame.new 'Panda Pal'
      unless Pref.ui[:frame_dimensions].nil?
        UI.frame.set_size *Pref.ui[:frame_dimensions]
      else
        UI.frame.set_size 600, 600
      end
      UI.frame.set_visible true
      UI.ui_loaded = true
    end
  end

  def UI.handle_exception(ex)
    puts ex.message
    puts ex.backtrace
    javax.swing.JOptionPane.show_message_dialog(UI.frame, 'Oops, something didn\'t go quite right during that operation. :(', 'Oops', javax.swing.JOptionPane::ERROR_MESSAGE)
  end

  def UI.control_mask
    Misc::OS.mac? ? KeyEvent::META_DOWN_MASK : KeyEvent::CTRL_DOWN_MASK
  end

end