java_import javax.swing.JPanel
java_import javax.swing.JProgressBar
java_import javax.swing.JLabel
java_import javax.swing.SpringLayout
java_import javax.swing.JOptionPane

class ProgressPanel < JPanel

  attr_accessor :sl
  attr_accessor :progress_bar
  attr_accessor :percentage_label
  attr_accessor :status_label
  attr_accessor :elapsed_label
  attr_accessor :estimated_label

  def initialize
    super
    self.sl = SpringLayout.new
    set_layout sl
    do_layout
  end

  # params:
  # - :percentage (self explanatory, only used when :type is :known)
  # - :type (:known, or :unknown, used for progress bar indeterminate status)
  # - :elapsed (pre-formatted string, for elapsed time on current operation)
  # - :estimated (pre-formatted string, for estimated time remaining)
  # - :count (only considered when :type is :unknown)
  # - :status (describe the action being performed, i.e. 'Finding courses...')
  # TODO:
  # - :elapsed_seconds (number of seconds, handle formatting in this class)
  # - :estimated_seconds (number of seconds remaining, handle formatting in this class)
  def update(opts)
    progress_bar.set_indeterminate opts[:type] == :unknown
    if opts[:type] == :known
      percentage_label.set_text opts[:percentage].to_s + '%'
      progress_bar.set_value opts[:percentage]
    else
      percentage_label.set_text opts[:count].to_s + '/?'
    end
    status_label.set_text opts[:status] if opts[:status]
    elapsed_label.set_text opts[:elapsed]
    estimated_label.set_text opts[:estimated]
  end

  def finish
    progress_bar.set_indeterminate false
    progress_bar.set_value 100
  end

  private
  def do_layout
    self.progress_bar = JProgressBar.new
    add progress_bar

    self.percentage_label = JLabel.new '0%'
    add percentage_label

    self.status_label = JLabel.new 'Progress'
    add status_label

    self.elapsed_label = JLabel.new 'Time: 00:00:00'
    add elapsed_label

    self.estimated_label = JLabel.new 'ETA: 00:00:00'
    add estimated_label

    stop_btn = javax.swing.JButton.new 'Stop'
    stop_btn.add_action_listener do
      if UI.job_thread && UI.job_thread.alive?
        res = JOptionPane.show_confirm_dialog(UI.frame, 'Are you sure you want to stop the current process?', 'Confirm', JOptionPane::YES_NO_OPTION)
        # Double check the UI job thread here, as it may have terminated in the time it took to pass the confirm dialog
        UI.job_thread.exit if UI.job_thread && res == JOptionPane::YES_OPTION
      end
    end
    add stop_btn

    sl.put_constraint(SpringLayout::BASELINE, status_label, 0, SpringLayout::BASELINE, stop_btn)
    sl.put_constraint(SpringLayout::WEST, status_label, 5, SpringLayout::WEST, self)

    sl.put_constraint(SpringLayout::BASELINE, elapsed_label, 0, SpringLayout::BASELINE, stop_btn)
    sl.put_constraint(SpringLayout::EAST, elapsed_label, -5, SpringLayout::WEST, estimated_label)

    sl.put_constraint(SpringLayout::BASELINE, estimated_label, 0, SpringLayout::BASELINE, stop_btn)
    sl.put_constraint(SpringLayout::EAST, estimated_label, -15, SpringLayout::WEST, percentage_label)

    sl.put_constraint(SpringLayout::BASELINE, percentage_label, 0, SpringLayout::BASELINE, stop_btn)
    sl.put_constraint(SpringLayout::WEST, percentage_label, 0, SpringLayout::HORIZONTAL_CENTER, self)

    sl.put_constraint(SpringLayout::NORTH, progress_bar, 2, SpringLayout::NORTH, self)
    sl.put_constraint(SpringLayout::SOUTH, progress_bar, -2, SpringLayout::SOUTH, self)
    sl.put_constraint(SpringLayout::WEST, progress_bar, 5, SpringLayout::EAST, percentage_label)
    sl.put_constraint(SpringLayout::EAST, progress_bar, -5, SpringLayout::WEST, stop_btn)

    sl.put_constraint(SpringLayout::NORTH, stop_btn, 2, SpringLayout::NORTH, self)
    sl.put_constraint(SpringLayout::SOUTH, stop_btn, -2, SpringLayout::SOUTH, self)
    sl.put_constraint(SpringLayout::EAST, stop_btn, -5, SpringLayout::EAST, self)
  end
end