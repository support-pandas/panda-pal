java_import javax.swing.JLabel
java_import javax.swing.JRadioButton
java_import javax.swing.JTextField
java_import javax.swing.JButton
java_import javax.swing.ButtonGroup
java_import javax.swing.SpringLayout

module SearchPanel

  attr_accessor :search_field
  attr_accessor :search_btn

  def layout_search_panel(search_callback = nil)
    l = SpringLayout.new
    panel = JPanel.new
    panel.set_border javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.BevelBorder::RAISED)
    panel.set_layout l

    self.search_btn_group = ButtonGroup.new

    search_label = JLabel.new 'Search'
    panel.add search_label

    accounts_btn = JRadioButton.new 'Accounts', true
    panel.add accounts_btn
    search_btn_group.add accounts_btn

    courses_btn = JRadioButton.new 'Courses'
    panel.add courses_btn
    search_btn_group.add courses_btn

    sections_btn = JRadioButton.new 'Sections'
    panel.add sections_btn
    search_btn_group.add sections_btn

    users_btn = JRadioButton.new 'Users'
    panel.add users_btn
    search_btn_group.add users_btn

    self.search_field = JTextField.new
    search_field.add_action_listener { method(search_callback).call(selected_type, search_field.get_text) } if search_callback
    search_field.add_key_listener PalTextFieldKeyListener.new
    panel.add search_field

    self.search_btn = JButton.new 'Go'
    search_btn.add_action_listener { method(search_callback).call(selected_type, search_field.get_text) } if search_callback
    panel.add search_btn

    l.put_constraint(SpringLayout::NORTH, search_label, 5, SpringLayout::NORTH, panel)
    l.put_constraint(SpringLayout::WEST, search_label, 10, SpringLayout::WEST, panel)

    l.put_constraint(SpringLayout::NORTH, accounts_btn, 5, SpringLayout::SOUTH, search_label)
    l.put_constraint(SpringLayout::WEST, accounts_btn, 5, SpringLayout::WEST, panel)

    l.put_constraint(SpringLayout::NORTH, courses_btn, 5, SpringLayout::SOUTH, search_label)
    l.put_constraint(SpringLayout::WEST, courses_btn, 5, SpringLayout::EAST, accounts_btn)

    l.put_constraint(SpringLayout::NORTH, sections_btn, 5, SpringLayout::SOUTH, search_label)
    l.put_constraint(SpringLayout::WEST, sections_btn, 5, SpringLayout::EAST, courses_btn)

    l.put_constraint(SpringLayout::NORTH, users_btn, 5, SpringLayout::SOUTH, search_label)
    l.put_constraint(SpringLayout::WEST, users_btn, 5, SpringLayout::EAST, sections_btn)

    l.put_constraint(SpringLayout::NORTH, search_field, 5, SpringLayout::SOUTH, accounts_btn)
    l.put_constraint(SpringLayout::WEST, search_field, 5, SpringLayout::WEST, panel)
    l.put_constraint(SpringLayout::EAST, search_field, -5, SpringLayout::WEST, search_btn)

    l.put_constraint(SpringLayout::NORTH, search_btn, 0, SpringLayout::NORTH, search_field)
    l.put_constraint(SpringLayout::SOUTH, search_btn, 0, SpringLayout::SOUTH, search_field)
    l.put_constraint(SpringLayout::EAST, search_btn, -5, SpringLayout::EAST, panel)

    panel
  end

  def selected_type
    search_btn_group.get_elements.find { |b| b.is_selected }.get_text
  end

  private
  attr_accessor :search_btn_group

end