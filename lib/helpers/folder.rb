require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CanvasFolder < HelperBase

  UPDATE_URL = DESTROY_URL = '/folders/%id'

  def initialize(course_id = nil, user_id = nil, group_id = nil, folder_id = nil, attr = {}, new_obj = true)
    set_attribute(:course, course_id.is_a?(Course) ? course_id.id : course_id) if course_id
    set_attribute(:user, user_id.is_a?(User) ? user_id.id : user_id) if user_id
    set_attribute(:group, group_id) if group_id
    set_attribute(:folder, folder_id.is_a?(CanvasFolder) ? folder_id.id : folder_id) if folder_id
    self.create_url = self.get_url = '/courses/%course/folders' if get(:course)
    self.create_url = self.get_url ||= '/users/%user/folders' if get(:user)
    self.create_url = self.get_url||= '/groups/%group/folders' if get(:group)
    self.create_url = self.get_url ||= '/folders/%folder/folders' if get(:folder)
    super(attr, new_obj)
  end

  def upload_file(uri)
    return false unless Misc::check_safe_mode?(:file_upload, uri)
    c, code = Network::call('/folders/' + id.to_s + '/files', 'POST', { :file => uri }, true)
    c && code < 300 ? CanvasFile.new(c) : nil
  end

  def files(*attr)
    Batch.new Network::all_records('/folders/' + id.to_s + '/files', attr).collect { |f| CanvasFile.new(f) }
  end

  def folders
    Batch.new Network::all_records('/folders/' + id.to_s + '/folders').collect { |f| CanvasFolder.new(nil, nil, nil, id, f, false) }
  end

  def create_folder(attr = {})
    CanvasFolder.create(nil, nil, nil, id, attr)
  end

  def create_folder!(attr = {})
    CanvasFolder.create!(nil, nil, nil, id, attr)
  end
end