require './lib/helpers/helper_base'

class Submission < HelperBase
  GET_URL = UPDATE_URL = '/courses/%course/assignments/%assignment/submissions/%user'
  CREATE_URL = '/courses/%course/assignments/%assignment/submissions'

  def download(attr = {})
    attr[:prepend_user] = true if attr[:prepend_user].nil?
    attr[:get_users_name] = true if attr[:get_users_name].nil?
    stati = []
    user = User.find(user_id) if !!attr[:get_users_name] && get(:user_id)
    if get(:attachments)
      attachments.each do |a|
        stati << download_file(a['url'], a['filename'], attr, user)
      end
    end
    stati << download_file(media_comment['url'], (media_comment['display_name'] ? media_comment['display_name'] : media_comment['media_id']) + '.mp4', attr, user) if get(:media_comment)
    !stati.empty? && !stati.include?(false)
  end

  def self.upload_file(course, assignment, user, uri)
    return false unless Misc::check_safe_mode?(:file_upload, uri)
    c, code = Network::call('/courses/' + HelperBase.get_id(course).to_s + '/assignments/' +
      HelperBase.get_id(assignment).to_s + '/submissions/' + HelperBase.get_id(user).to_s + '/files', 'POST', { :file => uri }, true)
    c && code < 300 ? CanvasFile.new(c['id'], c, false) : nil
  end

  private
  def download_file(loc, filename, attr, user = nil)
    name = ((user ? user.name : get(:user_id).to_s) + '-' + filename) if !!attr[:prepend_user]
    name ||= filename
    if attr[:individual_folders]
      Network::file_download(loc, name,
                                      (attr[:directory] ? (attr[:directory] + '/') : './tmp/') << (user ? user.name : get(user_id).to_s))
    else
      Network::file_download(loc, name, (attr[:directory] ? (attr[:directory] + '/') : './tmp/'))
    end
  end

end
