require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class User < HelperBase

  CREATE_URL = '/accounts/self/users'
  GET_URL = '/users/%id'
  UPDATE_URL = '/users/%id'
  DESTROY_URL = '/accounts/self/users/%id'

  # Implements https://canvas.instructure.com/doc/api/users.html#method.profile.settings
  #
  # Returns a user with the given user ID
  def self.find(id)
    u, code = Network::call '/users/' + id.to_s + '/profile'
    code < 300 ? User.new(u['id'], u, false) : nil
  end

  # Implements https://canvas.instructure.com/doc/api/users.html#method.users.index
  #
  # Returns an array of users in a given account
  def self.list(account = 'self', search_term = nil)
    t = ['search_term=' + search_term] if search_term
    Batch.new Network::all_records('/accounts/' + account.to_s + '/users', (t ? t : [])).collect { |u| User.new(u['id'], u, false) }
  end

  # Implements https://canvas.instructure.com/doc/api/users.html#method.users.merge_into
  #
  # Merges two users together
  def merge(dest_usr, destination_account = nil)
    User.merge(self, dest_usr, destination_account)
    u, code = Network::call '/users/' + id.to_s + '/profile'
    update_object u
  end

  # Implements https://canvas.instructure.com/doc/api/users.html#method.users.merge_into
  #
  # Merges two users together
  def self.merge(source_usr, dest_usr, dest_account = nil)
    return false unless Misc::check_safe_mode?(:merge)
    s = source_usr.is_a?(User) ? source_usr.id : source_usr
    d = dest_usr.is_a?(User) ? dest_usr.id : dest_usr
    url = '/users/' + s.to_s + '/merge_into/' + d.to_s unless dest_account
    url ||= '/users/' + s.to_s + '/merge_into/accounts/' + dest_account.to_s + '/users/' + d.to_s
    Network::call url, 'PUT'
  end

  # Implements https://canvas.instructure.com/doc/api/logins.html#method.pseudonyms.index
  #
  # Returns an array of pseudonyms (or logins) for the user
  def logins
    call('/logins')[0]
  end

  def enrollments(*attr)
    # Set custom masquerading here, as this will return a 403 error otherwise
    Batch.new Network::all_records('/users/' + id.to_s + '/enrollments', masq? ? attr : attr.push('as_user_id=' + id.to_s)).collect { |e|
      Enrollment.new(id, e['course_id'], e['course_section_id'], e, false) }
  end

  def upload_file(uri)
    return false unless Misc::check_safe_mode?(:file_upload, uri)
    c, code = Network::call('/users/' + id.to_s + '/files', 'POST', { :file => uri }, true)
    c && code < 300 ? CanvasFile.new(c['id'], c, false) : nil
  end

  def file_quota
    c, code = Network::call '/users/' + id.to_s + '/files/quota'
    code < 300 ? c : nil
  end

  def files(*attr)
    Batch.new Network::all_records('/users/' + id.to_s + '/files', attr).collect { |f| CanvasFile.new(f['id'], f, false) }
  end

  def find_folder(folder)
    c, code = Network::call '/users/' + id.to_s + '/folders/' + folder.to_s
    c && code < 300 ? CanvasFolder.new(nil, id, nil, nil, c, false) : nil
  end

  def find_root_folder
    find_folder 'root'
  end

  def create_folder(attr = {})
    CanvasFolder.create(nil, id, nil, nil, attr)
  end

  def create_folder!(attr = {})
    CanvasFolder.create!(nil, id, nil, nil, attr)
  end

  def communication_channels
    Batch.new Network::all_records('/users/' + id.to_s + '/communication_channels').collect { |c| CommunicationChannel.new(id, c['id'], c, false) }
  end

  def create_communication_channel(attr = {})
    CommunicationChannel.new(id, attr)
  end

  def create_communication_channel!(attr = {})
    c = create_communication_channel attr
    c.update
    c
  end

  def find_communication_channel(com)
    CommunicationChannel.find(id, com)
  end

  def page_views(start_date = (Time.now - (60 * 60 * 24 * 30)), end_date = Time.now)
    Batch.new Network::all_records("/users/#{id.to_s}/page_views", ["start_date=#{start_date.to_s}", "end_date=#{end_date.to_s}"], false).collect { |v| PageView.new(id, v['id'], v, false) }
  end
end
