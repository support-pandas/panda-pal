require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class AssignmentGroup < HelperBase

  CREATE_URL = '/courses/%course/assignment_groups'
  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%course/assignment_groups/%id'

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.show
  #
  # Returns an assignment with the given course and assignment IDs
  #
  # Internal: This needs a custom override as it also fetches and batches assignments within the group
  def self.find(course, assignment)
    c_id = course.is_a?(Course) ? course.id : course
    c, code = Network::call('/courses/' + c_id.to_s + '/assignment_groups/' + assignment.to_s + '?include[]=assignments')
    if c && code < 300
      c['assignments'] = Batch.new(c['assignments'].collect { |a| Assignment.new(c_id, a['id'], a, false) })
      ag = AssignmentGroup.new(c_id, c['id'], c, false)
      return ag
    end
    nil
  end

end