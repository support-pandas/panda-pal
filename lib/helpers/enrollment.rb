require './lib/network.rb'
require './lib/helpers/helper_base'

class Enrollment < HelperBase

  GET_URL = '/accounts/self/enrollments/%id'
  DESTROY_URL = '/courses/%course/enrollments/%id'

  def initialize(user_id, course_id = nil, section_id = nil, attr = {}, new_obj = true)
    self.create_url = (section_id ? '/sections/%section' : '/courses/%course') + '/enrollments'
    args = [HelperBase.get_id(course_id)] unless section_id
    args ||= [HelperBase.get_id(section_id)]
    args << attr.merge({ user_id: HelperBase.get_id(user_id) })
    args << new_obj
    super(*args)
    set :course, HelperBase.get_id(course_id) if course_id
    set :id, attr['id'] unless new_obj
  end

  def self.create(user_id, course_id = nil, section_id = nil, attr = {})
    self.new(user_id, course_id, section_id, attr, true)
  end

  def self.create!(user_id, course_id = nil, section_id = nil, attr = {})
    c = create user_id, course_id, section_id, attr
    c.update
    c
  end

  def self.find(account = 'self', enrollment_id)
    c, code = Network::call '/accounts/' + account.to_s + '/enrollments/' + enrollment_id.to_s
    c && code < 300 ? Enrollment.new(c['user_id'], c['course_id'], c['course_section_id'], c, false) : nil
  end

  def conclude
    destroy('conclude')
  end

  def destroy(task = 'delete')
    destroy_inner(parse_urls(self.destroy_url), { :task => task })
  end

  def course
    Course.find get(:course_id)
  end

  def section
    Section.find get(:course_section_id)
  end

  def user
    User.find get(:user_id)
  end

end
