require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CommunicationChannel < HelperBase

  CREATE_URL = '/users/%user/communication_channels'
  GET_URL = DESTROY_URL = '/users/%user/communication_channels/%id'

  def self.find(user, addr)
    c = Network::all_records("/users/#{user.to_s}/communication_channels")
    c.select! { |c1| c1['address'] == addr || c1['id'] == addr }
    CommunicationChannel.new(user, c.first['id'], c.first, false) unless c.empty?
  end

  def notification_preferences
    Batch.new Network::call("/users/#{get(:user).to_s}/communication_channels/#{id.to_s}/notification_preferences")[0]['notification_preferences'].collect { |p| NotificationPreference.new(get(:user), id, p['notification'], p, false) }
  end

  def find_notification_preference(pref)
    NotificationPreference.find(get(:user), id, pref)
  end

end