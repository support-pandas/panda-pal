require './lib/network.rb'
require './lib/helpers/helper_base.rb'
require './lib/helpers/assignment'

class Course < HelperBase
  # Though courses can be published and unpublished, they don't follow the same format that is
  # consistent between assignments, quizzes, discussion topics, and module related items.
  # Because of this, Course handles its own publishing.

  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%id'
  CREATE_URL = '/accounts/%account/courses'

  CHILDREN = [
    Assignment
  ]

  # Stages a course for publishing
  def publish
    update_event :offer
  end

  # Stages a course for unpublishing
  def unpublish
    update_event :claim
  end

  # Immediately publishes a course
  def publish!
    publish
    update
  end

  # Immediately unpublishes a course
  def unpublish!
    unpublish
    update
  end

  def initialize(*attr)
    super(*attr)
    set :account, 'self'
  end

  # This is deprecated and should be removed
  def add(obj)
    case obj.class.name.downcase
      when 'assignment'
        obj.set_attribute(:course, self.id)
    end
    obj
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.settings
  #
  # Returns an array of course settings
  def settings
    c, code = Network::call '/courses/' + id.to_s + '/settings'
    code < 300 ? c : nil
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.update_settings
  #
  # Immediately pushes the specified settings to Canvas
  def update_settings(attr = {})
    c, code = Network::call('/courses/' + id.to_s + '/settings', 'PUT', attr)
    code < 300 ? c : nil
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.destroy
  #
  # Immediately concludes the course
  def conclude
    destroy 'conclude'
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.destroy
  #
  # Deletes item from Canvas immediately
  def destroy(event = 'delete')
    super({ :event => event })
  end

  def destroy!(event = 'delete')
    destroy event
    update
  end

  def update_event(event)
    @updated_attributes[:event] = event
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.index
  #
  # Returns an array of all courses in which the current user is enrolled
  def self.list(*attr)
    Batch.new Network::all_records('/courses', attr).collect { |c| Course.new(c['id'], c, false) }
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.index
  #
  # Returns an array of all assignments in the course
  def assignments(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/assignments', attr).collect { |a| Assignment.new(id, a['id'], a, false) }
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.show
  #
  # Returns an assignment with the given assignment ID
  def find_assignment(assignment)
    Assignment.find(self, assignment)
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
  #
  # Stages the item.  Call #update to push to Canvas.
  def create_assignment(attr = {})
    Assignment.create id, attr
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
  #
  # Pushes the new assignment immediately to Canvas
  def create_assignment!(attr = {})
    Assignment.create! id, attr
  end

  def assignment_groups(*attr)
    Batch.new(
      Network::all_records('/courses/' + id.to_s + '/assignment_groups', attr.push('include[]=assignments'), false).collect { |a|
        a['assignments'] = Batch.new(a['assignments'].collect { |b| Assignment.new(id, b['id'], b, false) })
        AssignmentGroup.new(id, a['id'], a, false)
      }
    )
  end

  def find_assignment_group(assignment)
    c = AssignmentGroup.find(id, assignment)
    c ? c : nil
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
  #
  # Stages the item.  Call #update to push to Canvas.
  def create_assignment_group(attr = {})
    AssignmentGroup.create id, attr
  end

  # Implements https://canvas.instructure.com/doc/api/assignments.html#method.assignments_api.create
  #
  # Pushes the new assignment immediately to Canvas
  def create_assignment_group!(attr = {})
    AssignmentGroup.create! id, attr
  end

  def quizzes(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/quizzes', attr).collect { |a| Quiz.new(id, a['id'], a, false) }
  end

  def find_quiz(quiz)
    Quiz.find(self, quiz)
  end

  def create_quiz(attr = {})
    Quiz.create id, attr
  end

  def create_quiz!(attr = {})
    Quiz.create! id, attr
  end

  # Implements https://canvas.instructure.com/doc/api/courses.html#method.courses.users
  #
  # Returns an array of all users in the course
  def users(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/users', attr).collect { |u| User.new(u['id'], u, false) }
  end

  def recent_students
    Batch.new Network::all_records('/courses/' + id.to_s + '/recent_students').collect { |u| User.new(u['id'], u, false) }
  end

  def user(user_id)
    c, code = Network::call '/courses/' + id.to_s + '/users/' + user_id.to_s
    c && code < 300 ? User.new(c['id'], c, false) : nil
  end

  def enrollments(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/enrollments', attr).collect { |e|
      Enrollment.new(e['user_id'], id, e['course_section_id'], e, false) }
  end

  def upload_file(uri)
    return false unless Misc::check_safe_mode?(:file_upload, uri)

    c, code = Network::call('/courses/' + id.to_s + '/files', 'POST', { :file => uri }, true)
    c && code < 300 ? CanvasFile.new(c['id'], c, false) : nil
  end

  def file_quota
    c, code = Network::call '/courses/' + id.to_s + '/files/quota'
    code < 300 ? c : nil
  end

  def files(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/files', attr).collect { |f| CanvasFile.new(f['id'], f, false) }
  end

  def folders(*attr)
    Batch.new Network::all_records('/courses/' + id.to_s + '/folders', attr).collect { |f| CanvasFolder.new(id.to_s, nil, nil, f['id'], f, false) }
  end

  def find_folder(folder)
    c, code = Network::call '/courses/' + id.to_s + '/folders/' + folder.to_s
    c && code < 300 ? CanvasFolder.new(id.to_s, nil, nil, nil, c, false) : nil
  end

  def find_root_folder
    find_folder 'root'
  end

  def create_folder(attr = {})
    CanvasFolder.create(id, nil, nil, nil, attr)
  end

  def create_folder!(attr = {})
    CanvasFolder.create!(id, nil, nil, nil, attr)
  end

  def sections
    # Override default pagination rules here.
    # Canvas doesn't use normal pagination when returning sections, and this call would
    # continue forever without stopping it
    Batch.new Network::all_records('/courses/' + id.to_s + '/sections', [], false).collect { |s| Section.new(id, s, false) }
  end

  def find_section(section_id)
    c, code = Network::call('/courses/' + id.to_s + '/sections/' + section_id.to_s)
    c && code < 300 ? Section.new(c['id'], c, false) : nil
  end

  def create_section(attr = {})
    Section.create(id, attr)
  end

  def create_section!(attr = {})
    Section.create!(id, attr)
  end

  def enroll_user(user, attr = {})
    Enrollment.create(user, id, nil, attr)
  end

  def enroll_user!(user, attr = {})
    Enrollment.create!(user, id, nil, attr)
  end

  def enroll_student(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'StudentEnrollment' }))
  end

  def enroll_student!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'StudentEnrollment' }))
  end

  def enroll_teacher(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'TeacherEnrollment' }))
  end

  def enroll_teacher!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'TeacherEnrollment' }))
  end

  def enroll_ta(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'TaEnrollment' }))
  end

  def enroll_ta!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'TaEnrollment' }))
  end

  def enroll_observer(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'ObserverEnrollment' }))
  end

  def enroll_observer!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'ObserverEnrollment' }))
  end

  def enroll_designer(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'DesignerEnrollment' }))
  end

  def enroll_designer!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'DesignerEnrollment' }))
  end

  def groups
    Batch.new Network::all_records('/courses/' + id.to_s + '/groups').collect { |g| Group.new(id, nil, g, false) }
  end

  def find_group(group)

  end

  def external_tools
    Batch.new Network::all_records('/courses/' + id.to_s + '/external_tools')
  end

  def students
    users 'enrollment_type=student'
  end

  def teachers
    users 'enrollment_type=teacher'
  end

  def tas
    users 'enrollment_type=ta'
  end

  def observers
    users 'enrollment_type=observer'
  end

  def designers
    users 'enrollment_type=designer'
  end

  def discussion_topics
    Batch.new Network::all_records('/courses/' + id.to_s + '/discussion_topics').collect { |d| CourseDiscussionTopic.new(id, d['id'], d, false) }
  end

  def create_discussion_topic(attr = {})
    CourseDiscussionTopic.create id, attr
  end

  def create_discussion_topic!(attr = {})
    d = create_discussion_topic attr
    d.update
    d
  end

  def find_discussion_topic(dis)
    CourseDiscussionTopic.find self, dis
  end

  def front_page
    c, code = Network::call('/courses/' + id.to_s + '/front_page')
    c && code < 300 ? CoursePage.new(id, c['url'], c, false) : nil
  end

  def pages
    Batch.new Network::all_records('/courses/' + id.to_s + '/pages').collect { |p| CoursePage.new(id, p['url'], p, false) }
  end

  def find_page(page)
    c, code = Network::call('/courses/' + id.to_s + '/pages/' + page.to_s)
    c && code < 300 ? CoursePage.new(id, c['url'], c, false) : nil
  end

  def create_page(attr = {})
    CoursePage.create self, attr
  end

  def create_page!(attr = {})
    p = create_page attr
    p.update
    p
  end

  # Content Migration

  # Implements https://canvas.instructure.com/doc/api/content_exports.html#method.content_exports_api.create'
  #
  # Export course content to common cartridge format
  def export(skip_notifications = false)
    binding.pry
    CanvasToolkit::ContentExport.new('/courses/' << id.to_s, call('/content_exports', 'POST', {
      'export_type' => 'common_cartridge',
      'skip_notifications' => skip_notifications
    })[0])
  end

  # Exports course quizzes to QTI .zip
  def export_quizzes

  end

  # Exports course files to a .zip file
  def export_files

  end

end
