require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class Conversations < HelperBase

  GET_URL = UPDATE_URL = DESTROY_URL = '/conversations/%id'
  CREATE_URL = '/conversations'

  # https://canvas.instructure.com/doc/api/conversations.html#method.conversations.mark_all_as_read
  #
  # Immediately marks a user's conversations as read
  def self.mark_all_as_read
    Network::call('/conversations/mark_all_as_read', 'POST')
  end

  def self.list(*attr)
    Batch.new Network::all_records('/conversations', attr).collect { |c| Conversations.new(c, false) }
  end
end
