require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CourseDiscussionTopicEntry < DiscussionTopicEntry

  CREATE_URL = '/courses/%course/discussion_topics/%discussion_topic/entries'
  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%course/discussion_topics/%discussion_topic/entries/%id'

end