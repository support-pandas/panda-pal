require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class GroupDiscussionTopicEntry < DiscussionTopicEntry

  CREATE_URL = '/groups/%group/discussion_topics/%discussion_topic/entries'
  GET_URL = UPDATE_URL = DESTROY_URL = '/groups/%group/discussion_topics/%discussion_topic/entries/%id'

end