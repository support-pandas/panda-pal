require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class DiscussionTopicEntry < HelperBase

  PERMANENTLY_STAGED = [:message]

  def replies
    Batch.new Network::all_records(parse_urls(self.class.const_get(:GET_URL)) + '/replies').collect { |e| self.class.new(get(:course), get(:discussion_topic), e['id'], e, false) }
  end

  def post_reply(message)
    r, code = Network::call(parse_urls(self.class.const_get(:GET_URL)) + '/replies', 'POST', { :message => message })
    code < 300 ? self.class.new(course? ? get(:course) : get(:group), get(:discussion_topic), r['id'], r, false) : nil
  end
  alias :post_reply! :post_reply

  # Custom find implementation, as the API doesn't provide one
  def self.find(context, topic, id)
    d = self == CourseDiscussionTopicEntry ? CourseDiscussionTopic.find(context, topic) : GroupDiscussionTopic.find(context, topic)
    e = d.entries
    e.select! { |b1| b1.id == id }
    e.length > 0 ? e.first : nil
  end

  def update(attr = {})
    masq(user_id) do
      return super.update attr
    end
  end

  private
  def course?
    self.class == CourseDiscussionTopicEntry
  end

  def group?
    !course?
  end
end
