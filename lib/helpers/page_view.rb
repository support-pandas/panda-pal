require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class PageView < HelperBase
  GET_URL = '/users/%user/page_views/%id'
end