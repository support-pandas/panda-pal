require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CoursePage < Page
  CREATE_URL = '/courses/%course/pages'
  UPDATE_URL = GET_URL = DESTROY_URL = '/courses/%course/pages/%url'

end