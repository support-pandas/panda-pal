require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class GroupPage < Page
  CREATE_URL = '/groups/%group/pages'
  UPDATE_URL = GET_URL = DESTROY_URL = '/groups/%group/pages/%url'
end