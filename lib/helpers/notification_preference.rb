require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class NotificationPreference < HelperBase

  GET_URL = '/users/%user/communication_channels/%communication_channel/notification_preferences/%id'
  UPDATE_URL = '/users/self/communication_channels/%communication_channel/notification_preferences/%id'

  def self.find(user, channel, id)
    c = Network::call("/users/#{user.to_s}/communication_channels/#{channel.to_s}/notification_preferences/#{id.to_s}")[0]['notification_preferences']
    c.select! { |c1| c1['notification'] == id.to_s }
    NotificationPreference.new(user, channel, c.first['notification'], c.first, false) unless c.empty?
  end

  def update(*attr, &block)
    raise 'This method will only succeed if you are masquerading as the attached user' if !masq? || masq_as != get(:user)
    super(*attr, &block)
  end

end