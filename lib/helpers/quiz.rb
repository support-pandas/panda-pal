require './lib/network.rb'
require './lib/helpers/helper_base.rb'
require './lib/mixins/publishable.rb'

class Quiz < HelperBase
  include Publishable

  CREATE_URL = '/courses/%course/quizzes'
  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%course/quizzes/%id'

  def assignment
    get(:assignment_id) ? Assignment.find(get(:course), get(:assignment_id)) : nil
  end

  def assignment?
    !!get(:assignment_id)
  end

  # this is a straight up copy of Network::all_records
  # unfortunately necessary due to the inconsistencies in the return for "quiz_submissions"
  # (all submissions are nested within a "quiz_submissions" key)
  def submissions
    page = 1
    per_page = '?per_page=' + Pref.config[:per_page].to_s
    result = []
    Progress.unknown_bar {
      loop do
        uri = parse_urls(get_url) << '/submissions' << per_page << '&page=' << page.to_s
        c, code = Network::call(uri)
        c = c['quiz_submissions']
        if code < 300
          result += c
          page += 1
          Progress.current_count = result.length
          Progress.format 'count' => (result.length).to_s
        end
        if c.length < Pref.config[:per_page] || code >= 300
          break
        end
      end
    }
    result
  end
end
