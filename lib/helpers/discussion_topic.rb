require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class DiscussionTopic < HelperBase
  include Publishable

  def entries
    ent = []
    Network::all_records(parse_urls(self.class.const_get(:GET_URL)) + "/entries").each do |v|
      ent += handle_recursive_entries(v)
    end
    Batch.new ent
  end

  def find_entry(entry)
    get_entry_class.find(course? ? get(:course) : get(:group), id, entry)
  end

  def post_entry(message)
    e, code = call('/entries', 'POST', { :message => message })
    code < 300 ? get_entry_class.new(course? ? get(:course) : get(:group), id, e['id'], e, false) : nil
  end

  protected
  def course?
    self.class == CourseDiscussionTopic
  end

  def group?
    !course?
  end

  def get_entry_class
    course? ? CourseDiscussionTopicEntry : GroupDiscussionTopicEntry
  end

  def handle_recursive_entries(val)
    entries = []
    if val['recent_replies']
      val['recent_replies'].each do |a|
        entries += handle_recursive_entries(a)
      end
    end
    entries << get_entry_class.new(course? ? get(:course) : get(:group), id, val['id'], val.reject { |k, v| k == 'replies' }, false)
    entries
  end
end
