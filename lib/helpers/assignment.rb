require './lib/network.rb'
require './lib/helpers/helper_base.rb'
require './lib/mixins/publishable.rb'

class Assignment < HelperBase
  include Publishable

  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%course/assignments/%id'
  CREATE_URL = '/courses/%course/assignments'

  def submissions(*attr)
    Batch.new Network::all_records('/courses/' + get(:course).to_s + '/assignments/' + id.to_s + '/submissions', attr).collect { |a| Submission.new(get(:course), id, a['user_id'], a, false) }
  end

  def find_submission(sub)
    Submission.find(get(:course), id, sub)
  end

  def create_submission(*attr)
    Submission.new get(:course), id, *attr
  end

  def create_submission!(*attr)
    s = create_submission *attr
    s.update
    s.set :user, s.user_id
    s
  end

  def upload_submission_file(user, uri)
    Submission.upload_file(get(:course), id, user, uri)
  end

  def upload_submission_file_and_submit!(user, uri, *attr)
    f = upload_submission_file(user, uri)
    create_submission!(attr.push({ :submission_type => 'online_upload', :file_ids => f.id }))
  end

  def quiz
    get(:quiz_id) ? Quiz.find(get(:course), get(:quiz_id)) : nil
  end

  def discussion_topic
    get(:discussion_topic) ? DiscussionTopic.find(get(:course), get(:discussion_topic)['id']) : nil
  end

  def quiz?
    !!get(:quiz_id)
  end

  def discussion_topic?
    !!get(:discussion_topic)
  end
end