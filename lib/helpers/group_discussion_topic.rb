require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class GroupDiscussionTopic < DiscussionTopic

  CREATE_URL = '/groups/%group/discussion_topics'
  GET_URL = UPDATE_URL = DESTROY_URL = '/groups/%group/discussion_topics/%id'

end