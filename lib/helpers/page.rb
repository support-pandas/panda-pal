require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class Page < HelperBase

  def revisions
    call('/revisions')
  end

  def revision(rev = 'latest')
    call('/revisions/' + rev.to_s)
  end

  def revert_to_revision(rev)
    call('/revisions/' + rev.to_s, 'POST')
  end
end