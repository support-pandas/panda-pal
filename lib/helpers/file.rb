require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CanvasFile < HelperBase

  GET_URL = UPDATE_URL = DESTROY_URL = '/files/%id'

  def public_url
    c, code = Network::call('/files/' + id.to_s + '/public_url')
    code < 300 ? c : nil
  end
end