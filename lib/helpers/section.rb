require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class Section < HelperBase

  CREATE_URL = '/courses/%course/sections'
  UPDATE_URL = GET_URL = DESTROY_URL = '/sections/%id'

  def initialize(*attr)
    super
    set :course, get(:course_id)
  end

  def crosslist(course)
    c, code = Network::call('/sections/' + id.to_s + '/crosslist/' + (course.is_a?(Course) ? course.id.to_s : course.to_s), 'POST')
    update_object(c) if c && code < 300
  end

  def decrosslist
    c, code = Network::call('/sections/' + id.to_s + '/crosslist', 'DELETE')
    update_object(c) if c && code < 300
  end
  alias :uncrosslist :decrosslist

  def crosslisted?
    !!@data['nonxlist_course_id']
  end

  def enrollments(*attr)
    Batch.new Network::all_records('/sections/' + id.to_s + '/enrollments', attr).collect { |e|
      Enrollment.new(e['user_id'], e['course_id'], id, e, false) }
  end

  # This method requires a somewhat unorthodox approach, simply because we can't directly find users in a section
  def users(*attr)
    u = course.users(*attr)
    e = enrollments
    u.select do |a|
      e.find_index { |b| a.id == b.get(:user_id) }
    end
  end

  def course
    Course.find get(:course_id)
  end

  def enroll_user(user, attr = {})
    Enrollment.create(user, get(:course), id, attr)
  end

  def enroll_user!(user, attr = {})
    Enrollment.create!(user, get(:course), id, attr)
  end

  def enroll_student(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'StudentEnrollment' }))
  end

  def enroll_student!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'StudentEnrollment' }))
  end

  def enroll_teacher(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'TeacherEnrollment' }))
  end

  def enroll_teacher!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'TeacherEnrollment' }))
  end

  def enroll_ta(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'TaEnrollment' }))
  end

  def enroll_ta!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'TaEnrollment' }))
  end

  def enroll_observer(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'ObserverEnrollment' }))
  end

  def enroll_observer!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'ObserverEnrollment' }))
  end

  def enroll_designer(user, attr = {})
    enroll_user(user, attr.merge({ :type => 'DesignerEnrollment' }))
  end

  def enroll_designer!(user, attr = {})
    enroll_user!(user, attr.merge({ :type => 'DesignerEnrollment' }))
  end
end