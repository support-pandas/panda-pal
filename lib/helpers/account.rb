require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class Account < HelperBase

  CREATE_URL = '/accounts/%account/sub_accounts'
  GET_URL = UPDATE_URL = '/accounts/%id'

  CHILDREN = [
    Account,
    Course
  ]

  def self.list(*attr)
    Batch.new Network::all_records('/accounts', attr).collect { |a| Account.new(a['id'], a, false) }
  end

  def self.root
    self.find('self')
  end

  def sub_accounts(*attr)
    Batch.new Network::all_records('/accounts/' + id.to_s + '/sub_accounts', attr).collect { |a| Account.new(a['id'], a, false) }
  end

  def courses(*attr)
    Batch.new Network::all_records('/accounts/' + id.to_s + '/courses', attr).collect { |c| Course.new(c['id'], c, false) }
  end

  def create_sub_account(attr = {})
    Account.create(self, attr)
  end

  def create_sub_account!(attr = {})
    Account.create!(self, attr)
  end

  def create_course(attr = {})
    c = Course.create(attr)
    c.set_attribute :account, id
    c
  end

  def create_course!(attr = {})
    create_course(attr).update
  end

  def users(*attr)
    Batch.new Network::all_records("/accounts/#{id.to_s}/users", attr).collect { |c| User.new(c['id'], c, false) }
  end
end