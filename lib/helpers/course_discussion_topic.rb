require './lib/network.rb'
require './lib/helpers/helper_base.rb'

class CourseDiscussionTopic < DiscussionTopic

  CREATE_URL = '/courses/%course/discussion_topics'
  GET_URL = UPDATE_URL = DESTROY_URL = '/courses/%course/discussion_topics/%id'

  def assignment
    get(:assignment_id) ? Assignment.find(get(:course), get(:assignment_id)) : nil
  end

  def assignment?
    !!get(:assignment_id)
  end
end