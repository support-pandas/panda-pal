require './lib/network'
require './lib/misc'

class HelperBase

  attr_accessor :get_url, :create_url, :update_url, :destroy_url
  protected :get_url, :create_url, :update_url, :destroy_url

  # #### Creates a new representation of an object.
  #
  # This method has 2 primary purposes.
  #
  # - Create a representation of an object that is pulled using the API.
  # - Create a new object, which is staged for a push to Canvas.
  #
  # **Arguments**
  #
  # - *route: parameters should be passed in the order they are required when using the API.
  #
  #     i.e.: to find an assignment, which has the path /courses/:course_id/assignments/:id, the call would look like 'Assignment.find(1, 2)'
  # - data: a hash of data with which to create the new object
  # - new_obj: whether this object is a representation of an existing object, or a new one entirely
  #
  # **Returns**
  #
  # - a representation of the object
  def initialize(*args) # :args: *route, data={}, new_obj=true

    # Batch fails if this isn't included
    args.collect! { |a| a.is_a?(HelperBase) ? HelperBase.get_id(a) : a }

    # Due to the way route_params are delivered, we need to make sure this has the proper hierarchy (which is flat)
    args.flatten! 1

    included_new_obj = args.last == !!args.last

    h = included_new_obj ? args[args.length - 2] : args.last
    included_hash = h.is_a? Hash
    h = h.is_a?(Hash) ? h : {}

    new_obj = included_new_obj ? args.last : true

    # Move this to its current location, to properly determine arity
    [:GET_URL, :CREATE_URL, :UPDATE_URL, :DESTROY_URL].each do |v|
      self.instance_variable_set('@' << v.to_s.downcase, self.class.const_get(v)) if self.class.constants.include?(v)
    end

    url = new_obj ? @create_url : @get_url
    raise RoutingError, 'This object cannot be ' << (new_obj ? 'created' : 'found') if url.nil?

    routes = HelperBase.get_routes url
    raise 'Not enough route parameters supplied' if routes.length < (args.length - (included_new_obj ? 1 : 0) - (included_hash ? 1 : 0))

    if args.length > (included_new_obj ? 1 : 0) + (included_hash ? 1 : 0)
      idx = 0

      routes.each do |r|
        set_attribute(r.first[1, r.first.length].dup, HelperBase.get_id(args[idx]))
        idx += 1
      end
    end

    @data = h
    @updated_attributes = new_obj ? h : {}
    @previous_attributes = {}

    # New object specifics
    if new_obj || h.empty?
      set_attribute :status, 'new'

      # Pulls attributes from Pref.attributes when creating an object shell
      # This will honor attributes that are supplied during the object creation and leave them be,
      # but will also find other attributes for the object, includes them, and sets them to nil.
      # Example:
      #
      # main> User.new
      # => #<User:0x0000
      #  @data = {
      #    "name" => nil,
      #    "unique_id" => nil
      #  }
      if Pref.attributes.keys.include? self.class.name
        @data = Hash[Pref.attributes[self.class.name].map do |k, v|
          # Only include writable attributes
          [k, nil] if %w(write_only read_write).include? v['access']
        end.compact].stringify_keys.merge @data.stringify_keys
      end
    end
  end

  # #### Finds a representation of an object, and returns it
  #
  # **Arguments**
  #
  # - *route: parameters should be passed in the order they are required when using the API.
  #
  #     i.e.: to find an assignment, which has the path /courses/:course_id/assignments/:id, the call would look like 'Assignment.find(1, 2)'
  #
  # **Returns**
  #
  # - a representation of the object if found, nil otherwise.
  def self.find(*attr) # :args: *route
    url = self.const_get(:GET_URL).dup if self.constants.include? :GET_URL
    raise 'This object cannot be found' if url.nil?
    idx = 0
    route_params = []
    HelperBase.get_routes(url).each do |r|
      raise 'A required parameter is missing: ' << r.first[1, r.first.length] if attr[idx].nil? || attr[idx].to_s.length == 0
      reg = Regexp.new r.first
      route_params << get_id(attr[idx]).to_s
      url.sub! reg, route_params.last
      idx += 1
    end

    c, code = nil, nil
    Progress.unknown_bar(title: "Finding #{self.to_s}...", type: :find) {
      c, code = Network::call url
    }
    if c && code < 300
      self.new(*route_params, c, false)
    else
      nil
    end
  end

  # #### Creates a new representation of an object.
  #
  # This method uses the same functionality as :;new, with an implicit new_obj=true flag
  #
  # **Arguments**
  #
  # - *route: parameters should be passed in the order they are required when using the API.
  #
  #     i.e.: to find an assignment, which has the path /courses/:course_id/assignments/:id, the call would look like 'Assignment.find(1, 2)'
  # - data: a hash of data with which to create the new object
  #
  # **Returns**
  #
  # - a representation of the object
  def self.create(*attr) # :args: *route, data={}
    self.new(*attr)
  end

  # Performs the same function as ::create, and immediately pushes any staged changes to Canvas.
  def self.create!(*attr) # :args: *route, data={}
    a = self.create(*attr)
    a.update
    a
  end

  # #### Pushes any staged changes to Canvas
  #
  # **Arguments**
  #
  # - attr: any parameters that should be used to update the object before being pushed.
  # - &block: a block that will be executed before being pushed.
  #
  # **Returns**
  #
  # - self
  def update(attr = {}, &block)
    if get(:status) == :deleted
      url = parse_urls(destroy_url)
      destroy_inner(url, attr)
    else
      raise 'This object cannot be updated.' if (new_obj? ? create_url.nil? : update_url.nil?)
      update_inner(attr, &block)
      url = parse_urls(new_obj? ? create_url : update_url)
      send(url)
    end
  end
  alias :save :update

  def update_inner(attr = {}, &block)
    @updated_attributes.merge! attr
    yield self if block_given?
  end
  protected :update_inner

  def update_object(json)
    reset
    @data = json
  end
  protected :update_object

  # #### Attempts to delete the object from Canvas
  #
  # **Arguments**
  #
  # - attr: any parameters that should be included in the payload
  #
  # **Returns**
  #
  # - true if the deletion is successful, false otherwise.
  #
  # [Link to endpoint in the api docs, if applicable](https://google.com)
  def destroy(attr = {})
    raise 'This object cannot be deleted.' if destroy_url.nil? || get(:status) == 'new'
    @destroy_attributes = attr
    set :status, :deleted
  end

  def destroy!(attr = {})
    destroy attr
    update
  end

  def destroy_inner(url, attr = {})
    attr.merge! @destroy_attributes if @destroy_attributes
    unless new_obj?
      unless get(:id)
        fallback_id = get(:url).to_s if self.is_a?(Page)
      end
      return unless Misc::check_safe_mode?('Are you sure you want to delete ' + self.class.name + ' ' + (@data['name'] ? ('"' + @data['name'] + '"') : (fallback_id ? fallback_id : id.to_s)) + '?')
      a, code = Network::call(url, 'DELETE', attr)
      update_object nil if code < 300
      return code < 300
    end
    false
  end
  protected :destroy_inner

  # Should only be used internally
  def self.load_attribute_methods
    Pref.attributes.each do |k, v|
      begin
        # Find the related class based on the items in attributes.yml
        klass = eval(k)
        # Iterate over each attribute belonging to the class
        v.each do |k1, v1|
          # Use reflection to call the private method :define_method
          # Reflect all the things!
          klass.send(:define_method, k1 + '=', lambda { |val|
            if v1['access'] == 'read_write' || v1['access'] == 'write_only'
              @previous_attributes[k1] = @data[k1]
              @data[k1] = val
              @updated_attributes[k1] = val
            else
              puts 'This attribute is read-only'
            end
          })
        end
      rescue

      end
    end
  end

  def require_attribute(*attr)
    attr.each do |a|
      if !@additional_attributes.include?(a) && !@data.include?(a)
        raise 'Attribute \'' + a.to_s + '\' is required but not specified'
      end
    end
  end
  protected :require_attribute

  # #### Set an additional attribute on the object
  #
  # This object differs from normal data assignment, as it adds additional data to the object, not necessarily part of the representation given from the API.
  #
  # **Arguments**
  #
  # - attr: the name of the key
  # - val: the value to be assigned to said key
  #
  # **Returns**
  #
  # - val
  def set_attribute(attr, val)
    @additional_attributes ||= {}
    @additional_attributes[attr.to_sym] = val
  end
  alias :set :set_attribute

  # #### Returns a hash of additional attributes set on the object.
  #
  # **Returns**
  #
  # - a hash of additional attributes
  def additional_attributes
    @additional_attributes ||= {}
    @additional_attributes
  end

  # #### Returns a hash of all pending updates.
  #
  # The returned hash is effectively all the data that will be pushed to Canvas when the object is updated.
  #
  # **Returns**
  #
  # - a hash of staged attributes
  def staged_update
    @updated_attributes.merge(permanently_staged)
  end
  alias :staged :staged_update

  # #### Removes all updated attributes from staging
  def reset
    @data.merge! @previous_attributes
    @previous_attributes = {}
    @updated_attributes = {}.merge(permanently_staged)
    @additional_attributes.delete :status if get(:status) == :deleted
    @destroy_attributes = nil
  end

  def permanently_staged
    if self.class.constants.include?(:PERMANENTLY_STAGED)
      h = {}
      self.class.const_get(:PERMANENTLY_STAGED).each do |p|
        h[p] = get(p)
      end
      return h
    end
    {}
  end
  private :permanently_staged

  # #### Used to determine whether the object is new, or whether it has an equivalent representation on Canvas.
  #
  # **Returns**
  #
  # - true if the object does not have a representation on Canvas, false otherwise.
  def new_object?
    additional_attributes[:status] == 'new'
  end
  alias :new_obj? :new_object?

  # #### Retrieves an attribute from the object.
  #
  # This differs from normal attribute retrieval, as it doesn't raise an exception if a value for the given key is not found.
  #
  # **Arguments**
  #
  # - attr: the key for which to provide a value
  #
  # **Returns**
  #
  # - an object if a value for the given key is found, nil otherwise.
  def get(attr)
    @data ||= {}
    additional_attributes[attr.to_sym] || additional_attributes[attr.to_s] || @data[attr.to_s] || @data[attr.to_sym]
  end

  # Uses etc/attributes.yml to scope object parameters correctly
  # for example, User.create 'unique_id' - 'unique_id' will be changed to 'user[unique_id]'
  def compile_hash
    h = {}
    @updated_attributes.merge(permanently_staged).each do |k, v|
      begin
        a = Pref.attributes[self.class.name]
        h[a[k.to_s]['api_name']] = v.to_s
      rescue NoMethodError
        raise CanvasToolkit::ConfigurationError, "Parameter #{k.to_s} not found. Please check etc/attributes.yml"
      end
    end
    h
  end
  protected :compile_hash


  # HTTP and URL Handling


  def send(url, method = nil)
    if new_obj?
      Pref.attributes[self.class.name].each do |k, v|
        if get(k).nil? && v['required']
          puts '\'' + k + '\' is a required parameter, but is missing'
          return
        end
      end
      tmp_json, code = Network::call(url, method ? method : 'POST', compile_hash)
      @additional_attributes.delete :status if code < 300
    else
      tmp_json, code = Network::call(url, method ? method : 'PUT', compile_hash)
    end
    update_object tmp_json if code < 300
  end
  protected :send

  def parse_urls(str)
    to_parse = [
        [/%account/, :account],
        [/%course/, :course],
        [/%section/, :section],
        [/%group/, :group],
        [/%folder/, :folder],
        [/%user/, :user],
        [/%assignment/, :assignment],
        [/%id/, :id],
        [/%discussion_topic/, :discussion_topic],
        [/%communication_channel/, :communication_channel],
        [/%url/, :url]
    ]
    to_parse.each do |i|
      str = str.gsub(i[0], get(i[1]).to_s) unless get(i[1]).nil?
    end
    r = str[/%\w+[^\/]/]
    raise ArgumentError, '\'' + r[0, r.length] + '\' is a required parameter, but is missing' if r
    str
  end
  protected :parse_urls

  def set_urls(urls)
    self.get_url = urls['get_url'] if urls['get_url']
    self.create_url = urls['create_url'] if urls['create_url']
    self.update_url = urls['update_url'] if urls['update_url']
    self.destroy_url = urls['destroy_url'] if urls['destroy_url']
  end
  protected :set_urls

  # #### Takes a URL and returns a parsed route
  def self.get_routes(url)
    url.scan(/(%\w+[^\/])/)
  end

  # #### Returns the representation of an object as a hash.
  #
  # **Returns**
  #
  # - a hash containing all representative data.
  def data
    @data
  end

  # #### Compares 2 objects
  #
  # **Arguments**
  #
  # - obj: the object to compare self against.
  #
  # **Returns**
  #
  # - an array of hashes containing differences between the 2 objects
  def diff(obj)
    raise TypeError.new('supplied objects are not same type') if (self.class != obj.class || !obj.is_a?(HelperBase))
    exclude = [:id]
    keys = self.data.keys | obj.data.keys
    d = { 'Objects' => ['object 1', 'object 2']}
    keys.each do |k|
      a = [self.get(k), obj.get(k)] if self[k] != obj[k] && !exclude.include?(k.to_sym) && !self[k].is_a?(Enumerable)
      d[k] = a if a
    end
    d.clear if d.length == 1 # Remove headers if no differences are found
    d
  end

  # #### Used as a general catch-all for attribute retrieval
  def method_missing(m, *args)
    m = m.to_s
    if m && @data.kind_of?(Enumerable) && @data.include?(m)
      @data[m]
    else
      raise MethodNotFound.new('Method not found: ' + m)
    end
  end

  # #### Retrieves an ID from a given object
  #
  # **Arguments**
  #
  # - obj: the object from which to extract an ID
  #
  # **Returns**
  #
  # - the ID of the object if said object is a subclass of HelperBase, the provided object otherwise.
  def self.get_id(obj)
    obj.is_a?(HelperBase) && obj.get(:id) ? obj.id : obj
  end

  # #### Allows calls to be stacked on top of defined by the object
  #
  # Be sure to include the initial /
  def call(url, method = 'GET', opts = {})
    u = get_url || update_url || destroy_url
    raise RoutingError, 'This object cannot be used for additional routing' unless u
    Network::call(parse_urls(u) << url, method, opts)
  end

  # #### Allows paginated GET calls to be called upon an object
  #
  # Be sure to include the initial /
  def all_records(uri, attr = [], paginate = true)
    u = get_url
    raise RoutingError, 'This object does not support GET calls' unless u
    Network::all_records(parse_urls(u) << uri, attr, paginate)
  end

  class << self
    alias_method :new!, :create!
    protected :method_missing
  end
end
