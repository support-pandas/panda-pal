require './lib/network.rb'
require './lib/helpers/helper_base'

class Group < HelperBase

  GET_URL = UPDATE_URL = DESTROY_URL = '/groups/%id'

  def discussion_topics
    Batch.new Network::all_records('/groups/' + id.to_s + '/discussion_topics').collect { |d| GroupDiscussionTopic.new(id, d['id'], d, false) }
  end

  def create_discussion_topic(attr = {})
    GroupDiscussionTopic.create id, attr
  end

  def create_discussion_topic!(attr = {})
    d = create_discussion_topic attr
    d.update
    d
  end

  def find_discussion_topic(dis)
    GroupDiscussionTopic.find self, dis
  end

  def front_page
    c, code = Network::call('/groups/' + id.to_s + '/front_page')
    c && code < 300 ? GroupPage.new(id, c['url'], c, false) : nil
  end

  def pages
    Batch.new Network::all_records('/groups/' + id.to_s + '/pages').collect { |p| GroupPage.new(id, p['url'], p, false) }
  end

  def find_page(page)
    c, code = Network::call('/groups/' + id.to_s + '/pages/' + page.to_s)
    c && code < 300 ? GroupPage.new(id, c['url'], c, false) : nil
  end

  def create_page(attr = {})
    GroupPage.create self, attr
  end

  def create_page!(attr = {})
    p = create_page attr
    p.update
    p
  end
end