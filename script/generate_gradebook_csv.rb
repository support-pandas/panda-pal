class GenerateGradebookCSV < Script
  describe name: :generate_gradebook_csv,
           short_name: :gradebook_csv,
           version: 0.1,
           author: 'byoung',
           description: 'Generates a gradebook CSV using submissions API'

  def initialize
    super
    req :course
    req :include_sis_ids
  end

  def sis?
    Misc::TRUTHY.include? data[:include_sis_ids]
  end

  def proper_sections_name(student, students, sections)
    # students in this context are really enrollments
    short_students = students.select { |a| a.user_id == student.user_id }
    short_sections = sections.select { |k, v| short_students.any? { |b| b.course_section_id == v.id } }.values
    case short_sections.length
    when 1
      short_sections.first.name
    when 2
      short_sections.map(&:name).join ' and '
    else
      short_sections.map(&:name)[0, short_sections.length - 1].join(', ') + ', and ' + short_sections.last.name
    end
  end

  def run
    require 'csv'
    c = Course.find data[:course]
    @weighted = c.apply_assignment_group_weights
    puts 'Finding assignments'
    assignments = c.assignments.to_a.select { |a| a.published? && a.grading_type != 'not_graded' }
    @assignment_groups = c.assignment_groups.to_a

    puts 'Finding students'
    students = c.enrollments('type[]=StudentEnrollment', 'state[]=active')

    csv = CSV.open('./tmp/' + c.name + '_gradebook.csv', 'w')
    headers = (sis? ? ['Student', 'ID', 'SIS User ID', 'SIS Login ID', 'Section'] : ['Student', 'ID', 'Section']) + assignments.collect { |a| a.name + " (#{a.id})" }
    headers += @assignment_groups.collect { |a| [a.name + ' Current Score', a.name + ' Final Score'] }.flatten + ['Current Score', 'Final Score']
    muted_row = (sis? ? ['', '', '', '', ''] : ['', '', '']) + assignments.collect { |a| !!a.muted ? 'Muted' : nil } + ([nil] * ((1 + @assignment_groups.length) * 2))
    points_possible_row = ['    Points Possible', '', ''] + (sis? ? ['', ''] : []) + assignments.collect { |a| a.points_possible }
    points_possible_row += ['(read only)'] * (1 + @assignment_groups.length) * 2
    csv << headers
    csv << muted_row
    csv << points_possible_row
    data = {}
    sections = {}
    grades_hash = {}
    current_scores = {}
    final_scores = {}

    puts 'Finding sections'
    students.each { |s| sections[s.course_section_id] = Section.find(s.course_section_id) if sections[s.course_section_id].nil? }
    students.uniq { |s| s.user_id }.each do |s|
      if sis?
        data[s.user_id] = [s.get(:user)['sortable_name'], s.user_id, s.get(:user)['sis_user_id'], s.get(:user)['sis_login_id'], proper_sections_name(s, students, sections)]
      else
        data[s.user_id] = [s.get(:user)['sortable_name'], s.user_id, proper_sections_name(s, students, sections)]
      end
      grades_hash[s.user_id] = {}
      current_scores[s.user_id] = {}
      final_scores[s.user_id] = {}
    end
    students.uniq! { |s| s.user_id }

    assignments.each_with_index do |a, i|
      puts "Processing assignment #{i + 1}/#{assignments.length}"
      sub = a.submissions
      sub.each do |s|
        if data[s.user_id]
          data[s.user_id] << s.score
          grades_hash[s.user_id][a.id] = s.score
        end
      end
      data.each do |k, v|
        data[k] << nil if v.length <= (i + (sis? ? 5 : 3))
      end
    end

    # Calculates read-only columns
    @assignment_groups.each do |ag|
      students.each do |s|
        scores = []
        possible = []
        total_possible = []
        total_points = 0
        assignments.each do |a|
          if get_group(a.assignment_group_id) == ag
            next if ag.rules['never_drop'] && ag.rules['never_drop'].include?(a.id)
            scores << grades_hash[s.user_id][a.id]
            possible << a.points_possible if grades_hash[s.user_id][a.id]
            total_possible << a.points_possible
          end
        end
        tmp_scores = scores.compact

        scores.collect! { |sc| sc ? sc : 0 }
        if ag.rules['drop_lowest'] && ag.rules['drop_lowest'] < tmp_scores.length
          ag.rules['drop_lowest'].times do
            i = tmp_scores.find_index(tmp_scores.min)
            tmp_scores.delete_at i
            possible.delete_at i
          end
        end
        if ag.rules['drop_highest'] && ag.rules['drop_highest'] < tmp_scores.length
          ag.rules['drop_highest'].times do
            i = tmp_scores.find_index(tmp_scores.max)
            tmp_scores.delete_at i
            possible.delete_at i
          end
        end

        if ag.rules['drop_lowest'] && ag.rules['drop_lowest'] < scores.length
          ag.rules['drop_lowest'].times do
            i = scores.find_index(scores.min)
            scores.delete_at i
            total_possible.delete_at i
          end
        end
        if ag.rules['drop_highest'] && ag.rules['drop_highest'] < scores.length
          ag.rules['drop_highest'].times do
            i = scores.find_index(scores.max)
            scores.delete_at i
            total_possible.delete_at i
          end
        end
        if ag.rules['never_drop']
          ag.rules['never_drop'].each do |r|
            scores << (grades_hash[s.user_id][r] ? grades_hash[s.user_id][r] : 0)
            tmp_scores << grades_hash[s.user_id][r]
            pp = assignments.find { |aa| aa.id == r }.points_possible
            total_possible << pp
            possible << pp if grades_hash[s.user_id][r]
          end
        end

        tmp_scores.compact!
        current_scores[s.user_id][ag.id] = {}
        final_scores[s.user_id][ag.id] = {}
        current_scores[s.user_id][ag.id][:weight] = final_scores[s.user_id][ag.id][:weight] = ag.group_weight
        current_scores[s.user_id][ag.id][:score_average] = (sum(tmp_scores) / tmp_scores.length.to_f)
        final_scores[s.user_id][ag.id][:score_average] = (sum(scores) / scores.length.to_f)
        current_scores[s.user_id][ag.id][:total_average] = (sum(possible) / possible.length.to_f)
        final_scores[s.user_id][ag.id][:total_average] = (sum(total_possible) / total_possible.length.to_f)

        current_scores[s.user_id][ag.id][:score] = sum(tmp_scores)
        final_scores[s.user_id][ag.id][:score] = sum(scores)
        current_scores[s.user_id][ag.id][:total] = sum(possible)
        final_scores[s.user_id][ag.id][:total] = sum(total_possible)

        current_scores[s.user_id][ag.id][:percentage] = ((current_scores[s.user_id][ag.id][:score_average] / current_scores[s.user_id][ag.id][:total_average]) * 100.to_f).round(2)
        final_scores[s.user_id][ag.id][:percentage] = ((final_scores[s.user_id][ag.id][:score_average] / final_scores[s.user_id][ag.id][:total_average]) * 100.to_f).round(2)
        data[s.user_id] << (current_scores[s.user_id][ag.id][:percentage].finite? ? current_scores[s.user_id][ag.id][:percentage] : nil)
        data[s.user_id] << (final_scores[s.user_id][ag.id][:percentage].finite? ? final_scores[s.user_id][ag.id][:percentage] : nil)
      end
    end
    # Calculate totals
    students.each do |s|
      current = final = 0.to_f
      current_total = final_total = 0.to_f
      weights = total_weights = 0.to_f
      @assignment_groups.each do |ag|
        if @weighted
          current += (current_scores[s.user_id][ag.id][:percentage] * current_scores[s.user_id][ag.id][:weight].to_f) if current_scores[s.user_id][ag.id][:percentage].finite?
          final += (final_scores[s.user_id][ag.id][:percentage] * final_scores[s.user_id][ag.id][:weight].to_f) if final_scores[s.user_id][ag.id][:percentage].finite?
          weights += current_scores[s.user_id][ag.id][:weight].to_f if current_scores[s.user_id][ag.id][:percentage].finite?
          total_weights += final_scores[s.user_id][ag.id][:weight].to_f if final_scores[s.user_id][ag.id][:percentage].finite?
        else
          if current_scores[s.user_id][ag.id][:score]
            current += current_scores[s.user_id][ag.id][:score]
            current_total += current_scores[s.user_id][ag.id][:total]
          end
          if final_scores[s.user_id][ag.id][:score]
            final += final_scores[s.user_id][ag.id][:score]
            final_total += final_scores[s.user_id][ag.id][:total]
          end
        end
      end
      if @weighted
        data[s.user_id] << (current / [100, weights].min.to_f).round(1)
        data[s.user_id] << (final / [100, total_weights].min.to_f).round(1)
      else
        current = ((current / current_total) * 100.to_f).round(1)
        final = ((final / final_total) * 100.to_f).round(1)
        data[s.user_id] << (current.finite? ? current : nil)
        data[s.user_id] << (final.finite? ? final : nil)
      end
    end
    data.each do |k, v|
      csv << v
    end
    csv.close
  end

  def get_group(id)
    @assignment_groups.find { |a| a.id == id }
  end

  def sum(ary)
    a = 0.to_f
    ary.each { |elem| a += elem.to_f }
    a
  end
end