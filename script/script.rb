class Script

  attr_accessor :data
  attr_accessor :requires

  class << self
    attr_accessor :meta_tags
  end

  def self.meta(param, val = nil)
    self.meta_tags ||= {}
    if val
      self.meta_tags[param] = val
    else
      self.meta_tags[param]
    end
  end

  def self.describe(attr = {})
    self.meta_tags ||= {}
    self.meta_tags.merge! attr
  end

  def req sym
    self.requires << sym
  end

  def initialize
    self.data = {}
    self.requires = []
  end

  def self.run(script)
    ScriptLoader.run(script)
  end

  def self.get_name
    self.meta(:name) ? self.meta(:name).to_s.gsub(/_/, ' ').capitalize : self.name.gsub(/([A-Z])/, ' \1').strip
  end

  def self.select
    ScriptLoader.selector
  end

end