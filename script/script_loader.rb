require './script/script'
require 'rbconfig'

module ScriptLoader

  OUTPUT_METHODS = [:basic]

  class << self
    attr_accessor :output_method
  end

  def self.load
    ScriptLoader.output_method = :basic

    # Suppress deprecation warnings about config
    if RUBY_VERSION < '2.2.0'
      Object.send :remove_const, :Config
      Object.send :const_set, :Config, :RbConfig
    end

    # Evaluate all script files in ./script direcetory
    files = Dir.entries './script'

    # Remove all files that include only periods (Unix stuff, like ., ..)
    files.reject! { |f| f.match /^\.+$/ }

    # If we try to load script_loader, we'll get a stack overflow (yo dawg)
    files.each { |f| Kernel.load('./script/' << f) unless f == 'script_loader.rb' }

    @@scripts = []
    Module.constants.each do |m|
      m = Module.module_eval(m.to_s)

      # Only register subclasses of Script, and not Script itself
      @@scripts << m if m && m.respond_to?(:name) && m.ancestors.include?(Script) && m.name != 'Script'
    end
  end

  def self.selector
    output 'Scripts:'
    @@scripts.length.times { |s| output((s + 1).to_s << ': ' << @@scripts[s].get_name) }
    output ''
    output '0: Change environment'
    choice = input('Choose a script to load [1]', 1).to_i - 1
    if choice == -1
      select_environment
      selector
    else
      output 'Running script: ' << @@scripts[choice].get_name
      run_script @@scripts[choice]
    end
  end

  def self.select_environment
    output 'Environments:'
    Pref.config[:environments].each { |k, v| puts k.to_s }
    res = input 'Choose an environment [' + Pref.config[:environment].to_s + ']', Pref.config[:environment].to_s
    if set_env res.to_sym, true
      output ('Setting environment to: ' + res.to_s).green
    else
      output 'Given environment does not exist'.red
      select_environment
    end
  end

  def self.run(script)
    @@scripts.each do |s|
      if s.meta(:short_name) == script || s.meta(:name) == script
        run_script(s)
        return
      end
    end
    output 'Script not found by that name: ' << script.to_s
    selector
  end

  def self.run_script(klass)
    get_domain
    inst = klass.new
    inst.requires.each do |r|
      inst.data[r] = input 'Input ' << (r.is_a?(Symbol) ? r.to_s.gsub(/_/, ' ') : r.to_s)
    end
    time = Time.now
    Progress.suppress(:find) do
      inst.run
    end
    puts 'Finished in ' + (Time.now - time).to_i.to_s + ' seconds'
  end

  def self.get_domain
    d = input 'Input domain: [' << domain << ']:'
    if d.empty?
      output ('Using default domain: ' << domain).green
    else
      output domain(d).green
    end
  end

  def self.input(message, default = '')
    if ScriptLoader.output_method == :basic
      puts message
      putc '>'
      putc ' '
      inp = gets.chomp
    end

    return inp.empty? ? default : inp
  end

  def self.output(message)
    if ScriptLoader.output_method == :basic
      puts message
    end
  end

end