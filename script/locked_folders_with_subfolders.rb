class LockedFoldersWithSubfoldersReport < Script
  describe name: :locked_folders_with_subfolders_report,
    short_name: :locked_folders_report,
    version: 0.1,
    author: 'smiller',
    description: 'Creates a CSV containing any courses that have a locked or unpublished folder that contains a nested folder. Useful for finding courses affected by CNVS-19623.'


  def initialize
    super
    req :account
    req :term
    req :output_directory
  end

  # is folder unpublished or hidden?
  def folder_affected?(folders, folder_id)
    folder = folders.find { |f| f.get(:id) == folder_id }
    # unpublished == "locked"
    return folder.is_a?(CanvasFolder) && (folder.locked == true || folder.hidden == true)
  end

  # check if course is using Better Files
  def course_affected?(course)
    course.call('/features/enabled')[0].include?('better_file_browsing')
  end

  def check_subfolders(course)
    parents_checked = []
    result = []
    begin
      folders = course.folders
      folders.any? do |folder|
        parent_id = folder.parent_folder_id

        if parent_id && !parents_checked.include?(parent_id)
          # cache parent folder ids for performance
          parents_checked << parent_id
          if folder_affected?(folders, parent_id)
            # ensure teachers exist in course
            teacher_id = course.teachers.length > 0 ? course.teachers.first.id : 'N/A'
            result << [course.name, course.id, teacher_id, course.account_id]
            true
          else
            false
          end
        else
          false
        end
      end
    rescue CanvasError => e
      puts "An error occurred! Continuing..."
      puts e.message
      puts e.backtrace

      result << [course.name, course.id, 'ERROR: ', e.message]
    end
    result
  end

  def compose_report(courses)
    puts "Composing report... Please wait..."

    csv = [[
      'Course Name', 'Course ID', 'Instructor ID', 'Sub-Account ID'
    ]]

    courses.each do |course|
      if course_affected?(course)
        csv += check_subfolders(course)
      end
    end

    puts "Report finished!"
    return csv
  end

  def write_csv(filename, csv_array)
    require 'csv'
    puts "Composing CSV..."

    CSV.open(filename, 'wb') do |csv|
      csv_array.each { |row| csv << row }
    end
  end

  def run
    term = data[:term].to_s
    output_path = File.expand_path("folder_report-term_#{term}.csv", data[:output_directory])

    account = Account.find(data[:account])
    courses = account.courses('published=true', 'enrollment_term_id=' << term)

    write_csv(output_path, compose_report(courses))
    puts "Report complete!"
  end
end
