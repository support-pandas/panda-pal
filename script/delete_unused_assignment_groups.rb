class DeleteUnusedAssignmentGroups < Script

  describe name: :delete_unused_assignment_groups,
     short_name: :delete_assignment_groups,
     version: 0.1,
     author: 'byoung',
     description: 'Deletes all unused assignment groups in a course'

  def initialize
    super
    req :course
  end

  def run
    @course = Course.find data[:course]
    ag = @course.assignment_groups
    len = ag.length
    puts 'There ' << Misc.pluralize('is', len) << ' ' << len.to_s << ' assignment ' << Misc.pluralize('group', len) << ' total in this course'
    ag.select! { |a| a.assignments.empty? }
    if ag.empty?
      puts 'There are no empty assignment groups in this course'
      puts 'No action will be taken'
    else
      ag.destroy
    end
  end
end