class TeachersWithPublishedCoursesReport < Script
  describe name: :teachers_with_published_courses_report,
    short_name: :teachers_published_courses_report,
    version: 0.1,
    author: 'smiller',
    description: 'Composes 2 CSVs for a given sub-account, term, and output directory that lists A) Number of courses, B) Number of published courses, C) Number of unique teachers that have at least one published course, D) List of unique teachers with at least one published course.'

  def initialize
    super
    req :sub_account
    req :term
    req :output_directory
  end

  def getCourses(account, term)
    account.courses('enrollment_term_id=' << term.to_s).to_ary
  end

  def getPublished(courses)
    courses.reject { |course|
      course.workflow_state == 'unpublished'
    }
  end

  def getTeachers(courses)
    teachers = []
    teacher_ids = []
    courses.each { |course|
      course.teachers.to_ary.each { |t|
        if t.get(:id) && !teacher_ids.include?(t.id)
          teacher_ids << t.id
          teachers << [course.name, course.get(:sis_course_id), t.name, t.get(:sis_user_id), t.id]
        end
      }
    }
    teachers
  end

  def run
    require 'csv'

    data[:sub_account].split(',').map { |account|
      Account.find(account)
    }.each { |sub_account|
      suppressed_state = Pref.config[:suppress_progress]
      begin
        puts "Processing #{sub_account.name}..."
        courses = getCourses sub_account, data[:term]
        raise 'No courses found within sub-account ' << sub_account.name unless courses.length > 0

        Pref.config[:suppress_progress] << 'all'

        published_courses = getPublished courses
        raise 'No published courses found' unless published_courses.length > 0
        teachers = getTeachers published_courses

        timestamp = Time.now.getutc.to_s.gsub(/ |:/, '_')

        filename = "#{sub_account.name}_term-#{data[:term]}_%s_#{timestamp}.csv"

        # DATA CSV
        CSV.open(File.expand_path(filename % 'data', data[:output_directory]), 'wb') { |csv|
          csv << ['Total Courses', 'Total Published Courses', 'Unique Teachers in Published Courses']
          csv << [courses.length, published_courses.length, teachers.length]
        }

        # TEACHERS CSV
        CSV.open(File.expand_path(filename % 'teachers', data[:output_directory]), 'wb') { |csv|
          csv << ['Course Name', 'SIS Course ID', 'Teacher Name', 'Teacher SIS ID', 'Teacher User ID']
          teachers.each { |t|
            csv << t
          }
        }
      rescue Exception => e
        puts "An error has been encountered. Continuing..."
        puts e.message
        puts e.backtrace
      ensure
        Pref.config[:suppress_progress] = suppressed_state
      end
    }
  end
end
