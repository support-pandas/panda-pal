class SetAllNotificationPreferencesToNever < Script

  describe name: :set_all_notification_preferences_to_never,
           short_name: :notification_prefs_to_never,
           version: 0.1,
           author: 'byoung',
           description: 'Resurrects a deleted communication channel, sets all notification preferences to never, and deletes the channel when finished'

  def initialize
    super
    req :user
    req :email_address
  end

  def run
    Misc::check_safe_mode?('Are you sure you want to undertake this operation?') do
      masq(data[:user].to_i) do
        user = User.find data[:user]
        coms = user.communication_channels
        com = coms.select { |c| c.address == data[:email_address] }.first
        com ||= user.create_communication_channel! address: data[:email_address], type: :email, skip_confirmation: true
        com.notification_preferences.update frequency: :never
        com.destroy
      end
    end
  end

end