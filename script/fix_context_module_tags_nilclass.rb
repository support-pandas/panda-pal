class FixContextModuleTagsNilClass < Script

  describe name: :fix_context_module_tags_nil_class,
     short_name: :context_module_tags,
     version: 0.1,
     author: 'byoung',
     description: 'Fixes the assignments index page loading issue'

  def initialize
    super
    req :course
  end

  def run
    @course = Course.find data[:course]
    a = @course.assignments
    a.select! do |e|
      (e.submission_types.include?('online_quiz') && e.get(:quiz_id).nil?) ||
        (e.submission_types.include?('discussion_topic') && e.get(:discussion_topic).nil?)
    end
    len = a.length
    puts 'There ' << Misc.pluralize('is', len) << ' ' << len.to_s << ' affected ' << Misc.pluralize('assignment', len) << ' in this course'
    if len > 0
      a.destroy
    else
      puts 'No work to be done'
    end
  end
end