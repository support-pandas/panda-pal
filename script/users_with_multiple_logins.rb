class UsersWithMultipleLogins < Script
  describe name: :users_with_multiple_logins,
    short_name: :multiple_logins,
    version: 0.1,
    author: 'smiller',
    description: 'Composes a CSV of all users within an account that have one or more logins.'

  def initialize
    super
  end

  def writeCSV(filename, path, csv_array)
    require 'csv'
    CSV.open(File.expand_path(filename, path), 'wb') { |csv|
      csv_array.each { |row|
        csv << row
      }
    }
  end

  def run
    account = Account.find('self')
    puts "Finding users..."
    users = account.users

    mult_logins = [['name', 'user_id', 'login_count']]

    puts "Compiling list of users with multiple logins..."
    users.each { |u|
      logins = u.logins
      if (logins.length > 1)
        mult_logins << [u.name, u.id, logins.length]
      end
    }

    puts "Writing CSV file..."
    writeCSV("#{account.name}_mult_logins.csv", './tmp/', mult_logins)
  end
end
