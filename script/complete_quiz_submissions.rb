class CompleteQuizSubmissions < Script
  describe name: :complete_quiz_submissions,
    short_name: :quiz_subs,
    version: 0.1,
    author: 'smiller',
    description: 'Completes *every* in progress quiz submission for a given quiz.'

  def initialize
    super
    req :course
    req :quiz
  end

  def get_incomplete_submissions(quiz)
    quiz.submissions.select { |s| s["workflow_state"] == "untaken" }
  end

  def complete_submission(quiz, submission)
    quiz.call("/submissions/#{submission["id"]}/complete", "POST", attempt: submission["attempt"], validation_token: submission["validation_token"])
  end

  def run
    course = Course.find data[:course]
    quiz = course.find_quiz data[:quiz]

    subs = get_incomplete_submissions quiz
    subs.each { |sub|
      complete_submission quiz, sub
    }
  end
end
