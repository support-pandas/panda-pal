#!/bin/bash
cd `dirname $0`
echo 'Updating Panda Pal!'
git checkout master && git pull --rebase
echo 'Updating Panda Pal Documentation!'
./generate_doc.command
