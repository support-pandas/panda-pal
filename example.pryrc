Pry.config.command_prefix = ";"

scratch_editor = Pry::CommandSet.new do
  block_command "e" do |file = 'scratch.rb'|
    Pry.run_command(';edit "./tmp/' << file << '"')
  end

  block_command "l" do |file = 'scratch.rb'|
    target.eval('load "./tmp/' << file << '"')
  end
end

Pry::Commands.import scratch_editor
